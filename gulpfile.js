const gulp = require('gulp');
const spSave = require('gulp-spsave');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const rename = require('gulp-rename');
const sourcemaps = require('gulp-sourcemaps');
const plumber = require('gulp-plumber');

const babel = require('gulp-babel');

const creds = require('./settings.js');

gulp.task('minify-js',() => {
	return gulp.src('./scripts/**/*.js')
		.pipe(plumber((err) => {
			console.log('script task error');
			console.log(err);
			this.emit('end');
		}))
		.pipe(sourcemaps.init())
		.pipe(babel({
			presets: ['es2015']
		}))
		.pipe(uglify())
		.pipe(concat('RXPCreateSite.js'))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('./dest'))
});

gulp.task('spUpload', ['minify-js'], () => {
	return gulp.src("./dest/*.js")
		.pipe(plumber((err) => {
			console.log('script task error');
			console.log(err);
			this.emit('end');
		}))
		.pipe(babel({
			presets: ['es2015']
		}))
		.pipe(spSave({
			siteUrl: 'https://nsitechnology.sharepoint.com/sites/Engagement/ProjectPortal/',
			folder: 'SiteAssets/Javascripts',
			checkin: true,
			checkinType: 1
		}, creds
		));
});

gulp.task('default', () =>{
	return gulp.watch('./scripts/**/*.js', ['spUpload']);
});
