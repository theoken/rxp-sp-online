﻿if ((Get-Module Microsoft.Online.SharePoint.PowerShell).Count -eq 0) 
{
    Import-Module Microsoft.Online.SharePoint.PowerShell -DisableNameChecking
}

Set-ExecutionPolicy bypass -Force 

#array to hold DLL files path
[string []] $files = @("C:\Program Files\Common Files\microsoft shared\Web Server Extensions\16\ISAPI\Microsoft.SharePoint.Client.dll",
            "C:\Program Files\Common Files\microsoft shared\Web Server Extensions\16\ISAPI\Microsoft.SharePoint.Client.Runtime.dll",
            "C:\Program Files\Common Files\microsoft shared\Web Server Extensions\16\ISAPI\Microsoft.SharePoint.Client.Taxonomy.dll"
            );

#iterate the DLL and check if the files are exist
foreach($file in $files)
{
    Get-ChildItem $file
    
    if(Test-Path $file)
    {
        Add-Type -Path $file
    }
    else
    {
        write-host "file is not exist. Download the DLL files and put into the C:\Program Files\Common Files\microsoft shared\Web Server Extensions\15\ISAPI folder"
    }   
}

#--------------------------------------------------------------------------------------------------------------------------------------
#OPENCONNECTION
#This function is used when SP Admin need to run -SPO cmdlet such as create site Collections
#--------------------------------------------------------------------------------------------------------------------------------------
function openConnection()
{    
    $adminUrl = "https://nsitechnology-admin.sharepoint.com" #CHANGE this Url        
    Connect-SPOService -Url $adminUrl -Credential $username

    write-host "Connection has opened" -BackgroundColor Green -ForegroundColor Black
}

#IMPORTANT to change user's credentials
# Insert the credentials and the name of the admin site.
$Username="kenny.soetjipto@rxpservices.com"
$AdminPassword=Read-Host -Prompt "Password" -AsSecureString

#IMPORTANT to change this url to appropriate URL
$AdminUrl="https://nsitechnology.sharepoint.com"

#Create Client Context object
$ctx=New-Object Microsoft.SharePoint.Client.ClientContext($AdminUrl)
$ctx.Credentials = New-Object Microsoft.SharePoint.Client.SharePointOnlineCredentials($Username, $AdminPassword)
$ctx.RequestTimeout = 7200000; #Set the timeout to be 2 hours at least
$ctx.ExecuteQuery()

#Initialize Global variables
write-host "Initialize global variables" -BackgroundColor Cyan -ForegroundColor Black
$gWeb = $ctx.Web;
$ctx.Load($gWeb)
$ctx.ExecuteQuery()
write-host $gWeb.Title " has been initialized"
$gList = $ctx.Web.Lists;
$ctx.ExecuteQuery()
Write-Host $gList.ToString() " has been initialized"
$gField = $ctx.Web.Fields;

$ctx.Load($gField)
$ctx.ExecuteQuery()
write-host "Global Web.Field variable has been initialized"

#--------------------------------------------------------------------------------------------------------------------------------------
#Object declarations to create Lists in Marketing Site
#--------------------------------------------------------------------------------------------------------------------------------------

#--------------------------------------------------------------------------------------------------------------------------------------
#REFERENCELIBRARY
#Objects define to build lists in Reference Library site
#--------------------------------------------------------------------------------------------------------------------------------------
#Create Library List in Reference Library site
$RefLibDocUrl = "https://nsitechnology.sharepoint.com/sites/ReferenceLibrary"
$RefLibDocTitle = "Library" 
$RefLibDocDesc = "A document library which stores useful resources, and key metadata related."

#--------------------------------------------------------------------------------------------------------------------------------------
#MARKETING
#Objects define to build lists in Marketing site
#--------------------------------------------------------------------------------------------------------------------------------------
#Create Marketing Announcement list
$MarketingUrl = "https://nsitechnology.sharepoint.com/sites/MarketingAndCollateral"
$MarketingAnnounceTitle = @("Announcement")
$MarketingAnnounceDesc = @("Highlight important additional items or updated items.")

#Create Marketing document list
$MarketingDocTitle = @("Corporate Collateral", "Case Studies", "White Papers", "Consultant Profiles", "Templates", "Marketing.")
$MarketingDocDesc = @("A library which contains major corporate collaterals such as Sales Support collateral, brand guidelines and collateral, etc.",
                      "A library which contains case studies developed of work RXP has done for the previous clients. Searchable by client, service and or product.",
                      "It contains white papers written by RXP. Searchable by service and or product.",
                      "It contains consultant profiles from all practices or offices. This library is searchable by practice, region, and or capability.",
                      "It contains a branded document templates which will be used by consultants.",
                      "It comprises other Marketing assets which only accessible by Marketing department."
                      )

#Create Marketing Wiki list
$MarketingWikiTitle = @("Wiki")
$MarketingwikiDesc = @("A Marketing & Collateral wiki site for the marketing team to maintain useful information and guidance.")

#--------------------------------------------------------------------------------------------------------------------------------------
#PROJECTPORTAL
#Objects define to build lists in Project Portal site
#--------------------------------------------------------------------------------------------------------------------------------------
#Create Library List in Reference Library site
$ProjectPortalCustUrl = "https://nsitechnology.sharepoint.com/sites/Engagement/ProjectPortal"
$ProjectPortalCustTitle = "Projects" 
$ProjectPortalCustDesc = "A list of all RXP projects along with key metadata and hyperlink to the project site."

#--------------------------------------------------------------------------------------------------------------------------------------
#PROJECTTEMPLATE
#Objects define to build lists in Project Portal site
#--------------------------------------------------------------------------------------------------------------------------------------
#Custom list
$proTempUrl = "https://nsitechnology.sharepoint.com/sites/Engagement/ProjectTemplate"

$proTempCustomCustTitle = @("Assumptions", "Risk", "Project Phases", "Deliverable Type")
$proTempCustomCustDesc = @("The list which contains all assumptions regarding the delivery management of the project.",
                           "The list which contains all identified project risks.",
                           "Project phases / sprints used to group / search deliverables. Prepopulated but can be updated per project.",
                           "Document category used for grouping / searching deliverables. Prepopulated but can be updated per project. E.g. test plan, specification, etc.")
#Document List
$proTempDocCustTitle = @("Commercials", "Project Management", "Deliverables", "Reports", "Meetings", "External Documents", "Correspondence")
$proTempDocCustDesc = @("Documents govern the engagements such as contract, SOW.",
                        "Collections of project management documents.",
                        "All Deliverables and supporting documents such as design specifications, test plans, communication plans, etc.",
                        "All project reports.",
                        "All documents relating to project meetings – whether they be internal project team meetings or client meetings.",
                        "All non-RXP document used to support the project, including client supplied documentation.",
                        "Important emails relevant to the project.")

$proTempIssueTitle = @("Issues")
$proTempIssueDesc = @("it contains all identified project issues.")

$proTempTaskTitle = @("Actions")
$proTempTaskDesc = @("A project action register for project members.")

#--------------------------------------------------------------------------------------------------------------------------------------
#ENGAGEMENT
#Objects define to build lists in Engagement site
#--------------------------------------------------------------------------------------------------------------------------------------
$engagementUrl = "https://nsitechnology.sharepoint.com/sites/Engagement"

$engageDocTitle = @("Pre-Sales")
$engageDocDesc = @("Documents developed for potential clients as part of the pre-sales process. Includes company information, proposals, quotes and other associated documentation. Organised using Document Sets.
Major versions denote documents ready to go to the client (or already sent to the client).
")
#--------------------------------------------------------------------------------------------------------------------------------------
#CORPORATE
#Objects define to build lists in Engagement site
#--------------------------------------------------------------------------------------------------------------------------------------
$corpUrl = "https://nsitechnology.sharepoint.com/sites/Corporate"
$corpDocTitle = @("Policies", "Strategy");
$corpDocDesc = @("A central register of all RXP policies", 
                 "RXP strategic planning documents")

$corpAnnounceTitle = "Announcements"
$corpAnnounceDesc = "Basic RXP announcement"

$corpLocTitle = "Location"
$corpLocDesc = "List of RXP office locations and pertinent information about each"



#------------------------------------------------------------------------------------------
# ATTACH CONTENTTYPE TO LIST
#------------------------------------------------------------------------------------------
#Reference Library
$refUrl = "https://nsitechnology.sharepoint.com/sites/ReferenceLibrary"
$refListTitle = "Library"
$refCTName = "Reference Document"

#Corp Strategy List
$corpUrl = "https://nsitechnology.sharepoint.com/sites/Corporate"
$corpListTitle = "Strategy"
$corpCTName = "Strategy"

#Corp Annual Report List
$corpUrl = "https://nsitechnology.sharepoint.com/sites/Corporate"
$corpAnnualListTitle = "Annual Reports"
$corpCTAnnualName = "Annual Report"