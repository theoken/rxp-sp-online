﻿if ((Get-Module Microsoft.Online.SharePoint.PowerShell).Count -eq 0) 
{
    Import-Module Microsoft.Online.SharePoint.PowerShell -DisableNameChecking
}

Set-ExecutionPolicy bypass -Force 

#array to hold DLL files path
[string []] $files = @("C:\Program Files\Common Files\microsoft shared\Web Server Extensions\16\ISAPI\Microsoft.SharePoint.Client.dll",
            "C:\Program Files\Common Files\microsoft shared\Web Server Extensions\16\ISAPI\Microsoft.SharePoint.Client.Runtime.dll",
            "C:\Program Files\Common Files\microsoft shared\Web Server Extensions\16\ISAPI\Microsoft.SharePoint.Client.Taxonomy.dll"           
            );

#iterate the DLL and check if the files are exist
foreach($file in $files)
{
    Get-ChildItem $file
    
    if(Test-Path $file)
    {
        Add-Type -Path $file
    }
    else
    {
        write-host "file is not exist. Download the DLL files and put into the C:\Program Files\Common Files\microsoft shared\Web Server Extensions\15\ISAPI folder"
    }   
}

#--------------------------------------------------------------------------------------------------------------------------------------
#EXECTX
#function to define client object
#PARAMETERS: ctxParam
#--------------------------------------------------------------------------------------------------------------------------------------
function exeCtx([Microsoft.SharePoint.Client.ClientContext]$ctxParam)
{
    try
    {
        $ctxParam.ExecuteQuery()        
    }
    catch [Net.WebException]
    {
        write-host "Error execute QUery: $_.Exception.ToString()" -BackgroundColor Red -ForegroundColor Black
    }
}



#--------------------------------------------------------------------------------------------------------------------------------------
#GETGROUPS
#function to get a group of terms depend on parameters provided
#PARAMETERS: string
#--------------------------------------------------------------------------------------------------------------------------------------
function getGroups([string] $groupName)
{
    #$groupName = "RXP"    
    if(testTermStore($mms))
    {        
        $termstores = $mms.GetDefaultSiteCollectionTermStore()
        $ctx.Load($termstores)
        exeCtx $ctx

        #Get all groups
        $groups = $termstores.Groups
        $ctx.Load($groups)
        exeCtx $ctx

        #select 
        $rxpGroups = $groups | Where-Object{$_.Name -eq $groupName}
        $ctx.Load($rxpGroups)
        exeCtx $ctx

        $result = $rxpGroups | Where-Object{$_.IsSiteCollectionGroup -eq $false}
        $ctx.Load($result)
        exeCtx $ctx
        return $result
    }
}

#--------------------------------------------------------------------------------------------------------------------------------------
#GETTERMSETS
#Function to get the termSets based on the group specified in the parameter
#PARAMETERS: string
#--------------------------------------------------------------------------------------------------------------------------------------
function getTermSets([string]$groupNameParam)
{    
    $rxpGroup = getGroups $groupNameParam 
    $termSet = $rxpGroup.TermSets
    $ctx.Load($termSet)
    exeCtx $ctx

    return $termSet;
}

#--------------------------------------------------------------------------------------------------------------------------------------
#GETALLGROUPS
#function to get all groups from the term store
#Provide sitename, title and owner values as parameters
#--------------------------------------------------------------------------------------------------------------------------------------
function getAllGroups()
{   
    if(testTermStore($mms)) #test whether term store is accessible or not
    {
        $termstores = $mms.GetDefaultSiteCollectionTermStore()
        $ctx.Load($termstores)
        exeCtx $ctx

        $groups = $termstores.Groups
        $ctx.Load($groups)
        exeCtx $ctx
        
        return $groups
    }
}

#--------------------------------------------------------------------------------------------------------------------------------------
#SHOWGROUPS
#function to show all groups available from the term store
#Provide sitename, title and owner values as parameters
#--------------------------------------------------------------------------------------------------------------------------------------
function showGroups()
{
    if(testTermStore($mms))
    {
        $termstores = $mms.GetDefaultSiteCollectionTermStore()
        $ctx.Load($termstores)
        exeCtx $ctx

        $groups = $termstores.Groups
        $ctx.Load($groups)
        exeCtx $ctx

        $groups | select Name, Id 
    }
}

#--------------------------------------------------------------------------------------------------------------------------------------
#SHOWTERMSTORE
#function to show term store information
#Provide sitename, title and owner values as parameters
#--------------------------------------------------------------------------------------------------------------------------------------
function showTermStore()
{
    if(testTermStore($mms))
    {
        Write-Host "Getting Term Store Info" -BackgroundColor Cyan -ForegroundColor Black
        $termStores = $mms.GetDefaultSiteCollectionTermStore()
        $ctx.Load($termstores)
        exeCtx $ctx
        

        Write-Host "Connected to TermStore:`nName: $($termStores.Name)
        `tID: $($termStores.ID)" -BackgroundColor Cyan -ForegroundColor Black
    }
}

#--------------------------------------------------------------------------------------------------------------------------------------
#TESTSESSION
#function to check whether Metadata session is available
#Provide sitename, title and owner values as parameters
#PARAMETERS: TaxonomySession
#RETURN: TRUE / FALSE
#--------------------------------------------------------------------------------------------------------------------------------------
function testSession([Microsoft.SharePoint.Client.Taxonomy.TaxonomySession]$mmsParam)
{
    if(!$mmsParam.ServerObjectIsNull)
    {
        return $true
    }
    return $false
}

#--------------------------------------------------------------------------------------------------------------------------------------
#TESTTERMSTORE
#function to test whether term store is available or offline
#Provide sitename, title and owner values as parameters
#PARAMETERS: TaxonomySession
#--------------------------------------------------------------------------------------------------------------------------------------
function testTermStore([Microsoft.SharePoint.Client.Taxonomy.TaxonomySession] $mmsParam)
{
   $stores = $mmsParam.GetDefaultSiteCollectionTermStore()
   $ctx.Load($stores)
   exeCtx $ctx

   if($stores.IsOnline)
   {
        Write-Host "Term Store is exist" -BackgroundColor Cyan -ForegroundColor Black
        return $true;       
   }

   Write-Host "Term Store is not online"
   return $false   
}

#Define global variables
# Insert the credentials and the name of the admin site
$Username="kenny.soetjipto@rxpservices.com"
$AdminPassword=Read-Host -Prompt "Password" -AsSecureString
$AdminUrl="https://nsitechnology-admin.sharepoint.com/_layouts/15/termstoremanager.aspx"

#Define Client object with adminUrl -> ContentTypeHub site
$ctx=New-Object Microsoft.SharePoint.Client.ClientContext($AdminUrl)
$ctx.Credentials = New-Object Microsoft.SharePoint.Client.SharePointOnlineCredentials($Username, $AdminPassword)
$ctx.RequestTimeout = 7200000; 
$ctx.ExecuteQuery()

#Instantiate MMS session Object
$mms = [Microsoft.SharePoint.Client.Taxonomy.TaxonomySession]::GetTaxonomySession($ctx)
$ctx.Load($mms)
exeCtx $ctx

#Define group that want it to access which is RXP group
$rxpGroupName = "RXP"

$rxpTermSets = getTermSets $rxpGroupName

#Get the first level of the terms
$rxpTermsResult += $rxpTermSets.Name

#--------------------------------------------------------------------------------------------------------------------------------------
#RECURSIVEREADS
#function to read terms recursively when there is subchilds dynamically
#PARAMETERS: TermSetItem, int
#--------------------------------------------------------------------------------------------------------------------------------------
function recursiveReads([Microsoft.SharePoint.Client.Taxonomy.TermSetItem]$current, [int] $nestedLoop)
{
    $loop = $nestedLoop + 1;

    $ctx.Load($current.Terms)
    exeCtx $ctx

    $terms = $current.Terms #Second level of the terms
    $ctx.Load($terms)
    exeCtx $ctx
    
    #"`t"*$loop+$terms.Name
       
    $enum = $terms.GetEnumerator()
    
    while($enum.MoveNext())
    {
        $rxpSubTerms = $enum.Current;
        #$rxpSubTerms.Name #third level and more       

        if($rxpSubTerms.TermsCount -gt 0) #if there is more than 2 level of Terms
        {
            $rxpSubTerms.Name
            recursiveReads $rxpSubTerms $loop
        }
    }

    #$current.Terms = $current.Terms | select Name,Terms,TermsCount,Id
}

$rxpTermsResult = "Terms: `n"


foreach($rxpTerms in $rxpTermSets) #First level of the terms
{
    $ctx.Load($rxpTerms.Terms) #Instantiate the terms object    
    exeCtx $ctx
    
    #$rxpTermsResult += $rxpTerms.Name+ "`n"
        
    foreach($rxpSubTerms in $rxpTerms.Terms) #Second level of the terms
    {
        $ctx.Load($rxpSubTerms)
                
        $rxpTermsResult += $rxpSubTerms.Name+ "`n"
    }
}


for($i = 0; $i -lt $rxpTermSets.Count; $i++)
{
    $rxpTermsResult += $rxpTermSets[$i].Name+ "`n"
    
    $ctx.Load($rxpTermSets[$i].Terms)
    exeCtx $ctx

    foreach($x in $rxpTermSets[$i].Terms)
    {
        $ctx.Load($x)
        $x.Name        
    }
}

function GetTerms([Microsoft.SharePoint.Client.Taxonomy.Term] $term)
{
    $SubTerms = $term.Terms;
    $Context.Load($SubTerms)
    $Context.ExecuteQuery();

    Foreach ($SubTerm in $SubTerms)
    {
        $file.Writeline($SubTerm.Name + "," + $SubTerm.Id);
        GetTerms($SubTerm);
    }
}