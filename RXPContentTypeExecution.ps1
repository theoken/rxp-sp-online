﻿#Include RXPContentType.ps1 and RXPContentTypeObject.ps1
. "C:\projects\RxPInternalProductionScript\RXPContentType.ps1"

#Create Root RXP Content Type
createCT $rootRXPCT $rootRXPParent $RootRXPDescription

#Create first level sub-content type from the RXP Document content type
createRXPCTs $rxpFirstCTs $rootRXPCT $rxpFirstCTDescriptions

#Create sub content type from Contract Document content type
createRXPCTs $rxpContractCTs $rxpContractCTParent $rxpContractCTDescriptions 

#Create sub content type from Marketing document content type 
createRXPCTs $rxpMeetingCTs $rxpMeetingCTParent $rxpMeetingCTDescriptions

#Create sub content type from Project document content type
createRXPCTs $rxpProjectsCTs $rxpProjectsCTParent $rxpProjectsCTsDescriptions

createRXPCTs $rxpProjectReportCTs $rxpProjectReportParent $rxpProjectReportCTsDescriptions

#Create sub content type from Marketing document content type
createRXPCTs $rxpMarketingCTs $rxpMarketingCTParent $rxpMarketingCTsDescriptions

#Create sub content type for RXP Video content type
createRXPCTs $rxpVideoCT $rxpVideoCTParent $rxpVideoCTDescription

#Create sub content type for RXP Image content type
createRXPCTs $rxpImageCT $rxpImageCTParent $rxpImageCTDescriptions

#Create sub content type for RXP Personnel content type
createRXPCTs $rxpPersonnelDocument $rxpPersonnelCTParent $rxpPersonnelDocumentDescription

#Create sub content type for RXP Employment Contract
createRXPCTs $rxpEmploymentContract $rxpEmploymentContractParent $rxpEmploymentContractDescription

#Create root RXP Document Set which inherit from Document Set 
createCT $rxpDocumentSet $rxpDocumentSetParent $rxpDocumentSetDescription

#Create RXP Contract Document Set which inherit from RXP Document Set
createCT $rxpContractDocumentSet $rxpContractDocumentSetParent $rxpContractDocumentSetDescription

#Create RXP Meeting Document Set which inherit from RXP Document Set
createCT $rxpMeetingDocumentSet $rxpMeetingDocumentSetParent $rxpMeetingDocumentSetDescription

#Create RXP Personnel record Set which inherit from RXP Document Set
createCT $rxpPersonnelRecordSet $rxpPersonnelRecordSetParent $rxpPersonnelRecordSetDescription

#Create Marketing Brief Content type
createRXPCTs $rxpMarketingBriefCT $rxpMarketingBriefCTParent $rxpMarketingBriefCTDescription

#Create RXP Corporate Policies content type
createCT $rxpCorpPolicyCT $rxpCorpCTParent $rxpCorpPolicyDesc

#Create RXP Corporate Strategy content type
createCT $rxpCorpStrategyCT $rxpCorpStrategyCTParent $rxpCorpStrategyDesc

#Create  RXP Annual Report content type
createCT $rxpCorpAnnualCT $rxpCorpAnnualCTParent $rxpCorpAnnualCTDesc