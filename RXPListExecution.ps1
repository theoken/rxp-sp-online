﻿. "C:\projects\RxPInternalProductionScript\RXPLists.ps1"

#------------------------------------------------------------------------------------------
# MARKETING
#------------------------------------------------------------------------------------------
#Marketing Announcement List
AnnouncementList $MarketingAnnounceTitle $MarketingAnnounceDesc $MarketingUrl

#Marketing Document List
for($i = 0; $i -ne $MarketingDocTitle.Count; $i++)
{
    documentList $MarketingDocTitle[$i] $MarketingDocDesc[$i] $MarketingUrl
}

#Marketing Wiki List
WikiList $MarketingWikiTitle $MarketingwikiDesc $MarketingUrl

#------------------------------------------------------------------------------------------
# PROJECT PORTAL
#------------------------------------------------------------------------------------------
#Project Portal List
customList $ProjectPortalCustTitle $ProjectPortalCustDesc $ProjectPortalCustUrl

#------------------------------------------------------------------------------------------
# PROJECT TEMPLATE
#------------------------------------------------------------------------------------------
#Document list
for($i = 0; $i -ne $proTempDocCustTitle.Count; $i++)
{
    documentList $proTempDocCustTitle[$i] $proTempDocCustDesc[$i] $proTempUrl
}

#Custom list
for($i = 0; $i -ne $proTempCustomCustTitle.Count; $i++)
{
    documentList $proTempCustomCustTitle[$i] $proTempCustomCustDesc[$i] $proTempUrl
}

#Issue Track List
issueTrackList $proTempIssueTitle $proTempIssueDesc $proTempUrl

#Task List
taskList $proTempTaskTitle $proTempTaskDesc $proTempUrl

#------------------------------------------------------------------------------------------
# ENGAGEMENT
#------------------------------------------------------------------------------------------
documentList $engageDocTitle $engageDocDesc $engagementUrl

#------------------------------------------------------------------------------------------
# CORPORATE
#------------------------------------------------------------------------------------------
#Corporate Document List
for($i = 0; $i -ne $corpDocTitle.Count; $i++)
{
    documentList $corpDocTitle[$i] $corpDocDesc[$i] $corpUrl
}

#Corporate announcement List
AnnouncementList $corpAnnounceTitle $corpAnnounceDesc $corpUrl

#Location
customList $corpLocTitle $corpLocDesc $corpUrl

#------------------------------------------------------------------------------------------
# ENABLE ALLOW CONTENT TYPE BASED ON URL
#------------------------------------------------------------------------------------------
#ENGAGEMENT
enableCTAllLists https://nsitechnology.sharepoint.com/sites/Engagement/Clients
enableCTAllLists https://nsitechnology.sharepoint.com/sites/Engagement/Lessons
enableCTAllLists https://nsitechnology.sharepoint.com/sites/Engagement/ProjectPortal
enableCTAllLists https://nsitechnology.sharepoint.com/sites/Engagement/ProjectTemplate

#Marketing
enableCTAllLists https://nsitechnology.sharepoint.com/sites/MarketingAndCollateral

#Reference Library
enableCTAllLists https://nsitechnology.sharepoint.com/sites/ReferenceLibrary
#Corporate Site
enableCTAllLists https://nsitechnology.sharepoint.com/sites/Corporate

#------------------------------------------------------------------------------------------
# ATTACH CONTENTTYPE TO LIST
#------------------------------------------------------------------------------------------
#Reference Library
addCTtoList $refUrl $refListTitle $refCTName

#Corporate Strategy List
addCTtoList $corpStrategyUrl $corpListTitle $corpCTName

#Corporate Annual Report List
addCTtoList $corpUrl $corpAnnualListTitle $corpCTAnnualName