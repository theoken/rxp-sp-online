﻿<#-------------------------------------------------------------------------------------------------------------------
Create Root sites for the Engagement site
--------------------------------------------------------------------------------------------------------------------#>
#Create Marketing Site with Team template and quota 20 GB
teamSite "MarketingAndCollateral" "Marketing and Collateral" "kenny.soetjipto@rxpservices.com"

#Create Reference Library Site with Team template and quota 20 GB
teamSite "ReferenceLibrary" "Reference Library" "kenny.soetjipto@rxpservices.com"

#Create Reference Library Site with Team template and quota 20 GB
teamSite "Engagement" "Engagement" "kenny.soetjipto@rxpservices.com"

teamSite "Corporate" "Corporate" "kenny.soetjipto@rxpservices.com" 

#Create People and Talent Site with Team template and quota 20 GB
teamSite "PeopleandTalent" "People and Talent" "kenny.soetjipto@rxpservices.com"

#Create Clients subsite with parent of the Engagement
createSubSite "Clients" "https://nsitechnology.sharepoint.com/sites/Engagement" "Clients" 

<#-------------------------------------------------------------------------------------------------------------------
Create Subsites for the Engagement site
--------------------------------------------------------------------------------------------------------------------#>

#Create Clients subsite with parent of the Engagement
createSubSite "Project Portal" "https://nsitechnology.sharepoint.com/sites/Engagement" "ProjectPortal"

#Create Clients subsite with parent of the Engagement. Title, URL, SiteName
createSubSite "Project Template" "https://nsitechnology.sharepoint.com/sites/Engagement" "ProjectTemplate" 

#Create Lessons subsite with parent of the Engagement. Title, URL, SiteName
createSubSite "Lessons" "https://nsitechnology.sharepoint.com/sites/Engagement" "Lessons"



