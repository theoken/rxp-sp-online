﻿#-------------------------------------------------------------------------------------------------------------------------------------- 
# Author   : Kenny Soetjipto
# Job Title: SharePoint Developer
# Company  : RXPServices
#-------------------------------------------------------------------------------------------------------------------------------------- 

#-------------------------------------------------------------------------------------------------------------------------------------- 
# Description: 
# This file contains initial global variable declaration
# It has connection function which connect to SP Online
# It has open connection which is needed to execute any -spo cmdlet
# By default, connect to root Site Collection in global URL:
#-------------------------------------------------------------------------------------------------------------------------------------- 

#--------------------------------------------------------------------------------------------------------------------------------------
# Functions:
# CONNECTSPONLINE, OPENCONNECTION
#--------------------------------------------------------------------------------------------------------------------------------------

#--------------------------------------------------------------------------------------------------------------------------------------
# Parameters /variables need to change:
# $gUrl, $username, $adminUrl
#--------------------------------------------------------------------------------------------------------------------------------------

if ((Get-Module Microsoft.Online.SharePoint.PowerShell).Count -eq 0) 
{
    Import-Module Microsoft.Online.SharePoint.PowerShell -DisableNameChecking
}

Set-ExecutionPolicy bypass -Force 

#array to hold DLL files path
[string []] $files = @("C:\Program Files\Common Files\microsoft shared\Web Server Extensions\15\ISAPI\Microsoft.SharePoint.Client.dll",
            "C:\Program Files\Common Files\microsoft shared\Web Server Extensions\15\ISAPI\Microsoft.SharePoint.Client.Runtime.dll",
            "C:\Program Files\Common Files\microsoft shared\Web Server Extensions\15\ISAPI\Microsoft.SharePoint.Client.Taxonomy.dll"  
            );

#iterate the DLL and check if the files are exist
foreach($file in $files)
{
    Get-ChildItem $file
    
    if(Test-Path $file)
    {
        Add-Type -Path $file
    }
    else
    {
        write-host "file is not exist. Download the DLL files and put into the C:\Program Files\Common Files\microsoft shared\Web Server Extensions\15\ISAPI folder"
    }   
}

#--------------------------------------------------------------------------------------------------------------------------------------
#Global variables declaration
#--------------------------------------------------------------------------------------------------------------------------------------
$gUrl = "https://nsitechnology.sharepoint.com" #CHANGE THIS URL
$secPass = ConvertTo-SecureString $password -AsPlainText -Force
$username = "kenny.soetjipto@rxpservices.com"; #CHANGE THIS USERNAME

#--------------------------------------------------------------------------------------------------------------------------------------
#Sharepoint Object declaration
#--------------------------------------------------------------------------------------------------------------------------------------
$cContext = New-Object Microsoft.SharePoint.Client.ClientContext($gUrl);
#change the time out to be 2 hours
$cContext.RequestTimeout = 7200000;
$credentials = New-Object Microsoft.SharePoint.Client.SharePointOnlineCredentials($username, $secPass)

#--------------------------------------------------------------------------------------------------------------------------------------
#OPENCONNECTION
#This function is used when SP Admin need to run -SPO cmdlet such as create site Collections
#--------------------------------------------------------------------------------------------------------------------------------------
function openConnection()
{    
    $adminUrl = "https://nsitechnology-admin.sharepoint.com" #CHANGE this Url        
    Connect-SPOService -Url $adminUrl -Credential $username

    write-host "Connection has opened" -BackgroundColor Green -ForegroundColor Black
}

#--------------------------------------------------------------------------------------------------------------------------------------
#CONNECTSPONLINE
#function to add [Text] SiteColumn
#it gives DisplayName and Name with same value(optional)
#It puts in Custom Column group(optional)
#--------------------------------------------------------------------------------------------------------------------------------------
function connectSPOnline($urlParam)
{
    Write-Host "Connecting to SharePoint Online" -BackgroundColor Cyan -ForegroundColor Black
    $cContext = New-Object Microsoft.SharePoint.Client.ClientContext($gUrl);
    $cContext.RequestTimeout = 7200000;
       
    Write-Host "Put credential into clientContext object" 
    $cContext.Credentials = $credentials
    $cContext.ExecuteQuery()  
}

#--------------------------------------------------------------------------------------------------------------------------------------
#instantiate Web and Lists object
#invoke connection to make sure that is connected. Otherwise, it will give error
#--------------------------------------------------------------------------------------------------------------------------------------
connectSPOnline $gUrl
openConnection

write-host "Initialize global variables" -BackgroundColor Cyan -ForegroundColor Black
$gWeb = $cContext.Web;
$cContext.load($gWeb)
$cContext.ExecuteQuery()

$gList = $cContext.Web.Lists;
$cContext.load($gList)
$cContext.ExecuteQuery()

$gField = $cContext.Web.Fields;
$cContext.Load($gField)
$cContext.ExecuteQuery()

