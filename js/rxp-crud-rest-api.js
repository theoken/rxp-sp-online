$(function () {
    "use strict";    
    $("#queryButton").click(queries);
    $("#createList").click(createList);
    $("#createItem").click(createItem);
    $("#updateItem").click(updateItems);
    $("#readHostWeb").click(callToHostWeb);
});

//queries Tasks list
function queries() {   
    
    var call = $.ajax({
        url: "../_api/web/Lists/getByTitle('Tasks')",
        type: "GET",
        dataType: "json",
        headers: {
            Accept: "application/json;odata=verbose"
        }
    });

    call.done(function (data, textStatus, jqXHR) {       
        var message = $("#message");
        message.text(data.d.Title);
    })

    call.fail(function (data, textStatus, errorM) {       
        var res = JSON.parse(jqXHR.responseText);
        var message = res ? res.error.message.value : textStatus;
        alert("Call failed " +message);
    })
}

//createList - tasks
function createList() {
    var call = $.ajax({
        url: "../_api/web/Lists",
        type: "POST",
        data: JSON.stringify({
            "__metadata": { type: "SP.List" },
            BaseTemplate: SP.ListTemplateType.tasks,
            Title: "Tasks"
        }),
        headers: {
            Accept: "application/json;odata=verbose",
            "Content-Type": "application/json;odata=verbose",
            "X-RequestDigest": jQuery("#__REQUESTDIGEST").val()
        }
    });

    call.done(function (data, textStatus, jqXHR) {
        var message = $("#message");
        message.text("List added");
    })

    call.fail(function (data, textStatus, errorM) {
        var res = JSON.parse(errorM.responseText);
        var message = res ? res.error.message.value : textStatus;
        alert("Call failed " + message);
    })
}

//createItem
function createItem() {
    var call = $.ajax({
        url: "../_api/web/?select=Title,CurrentUser/Id&$expand=CurrentUser/Id",
        type: "GET",
        dataType: "json",
        headers: {
            Accept: "application/json;odata=verbose"
        }
    });

    call.done(function (data, textStatus, jqXHR) {
        var userId = data.d.CurrentUser.Id;
        addItem(userId);
    });

    call.fail(function (data, textStatus, errorM) {
        var res = JSON.parse(jqXHR.responseText);
        var message = res ? res.error.message.value : textStatus;
        alert("Call failed " + message);
    })

    function addItem(userId) {
        var due = new Date();
        due.setDate(due.getDate() + 7);

        var call = $.ajax({
            url: "../_api/web/lists/getByTitle('Tasks')/Items",
            type: "POST",
            data: JSON.stringify({
                "__metadata": { type: "SP.Data.TasksListItem" },
                Title: "Sample Tasks",
                AssignedToId: userId,
                DueDate: due
            }),
            headers: {
                Accept: "application/json;odata=verbose",
                "Content-Type": "application/json;odata=verbose",
                "X-RequestDigest": jQuery("#__REQUESTDIGEST").val()
            }
        });
        call.done(function (data, textStatus, jqXHR) {
            var message = $("#message");
            message.text("Item added");
        });

        call.fail(function (data, textStatus, errorM) {
            var res = JSON.parse(jqXHR.responseText);
            var message = res ? res.error.message.value : textStatus;
            alert("Call failed " + message);
        });
    }
}

function updateItems() {

    var call = $.ajax({
        url: "../_api/web/lists/getByTitle('Tasks')/Items/?top=1",
        type: "GET",
        dataType: "json",
        headers: {
            Accept: "application/json;odata=verbose"
        }
    });

    call.done(function (data, textStatus, jqXHR) {        
        var items = data.d.results;
        if (items.length > 0) {
            var item = items[0];
            updateItem(item);
        }
    });

    call.fail(function (data, textStatus, errorM) {
        var res = JSON.parse(jqXHR.responseText);
        var message = res ? res.error.message.value : textStatus;        
        alert("Call failed " + message);
    });

    function updateItem(item) {
        var call = $.ajax({
            url: "../_api/web/lists/getByTitle('Tasks')/Items("+item.Id+")",
            type: "POST",
            data: JSON.stringify({
                "__metadata": { type: "SP.Data.TasksListItem" },
                Status: "In Progress",
                PercentComplete: 0.1
            }),
            headers: {
                Accept: "application/json;odata=verbose",
                "Content-Type": "application/json;odata=verbose",
                "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                "X-Http-Method": "PATCH",
                "IF-MATCH": item.__metadata.etag
            }
        });

        call.done(function (data, textStatus, jqXHR) {
            var message = $("#message");
            message.text("Item updated");
        });

        call.fail(function (data, textStatus, errorM) {
            var res = JSON.parse(jqXHR.responseText);
            var message = res ? res.error.message.value : textStatus;
            alert("Call failed " + message);
        });
    }
} //updateItems()

function callToHostWeb() {
    var hostUrl = decodeURIComponent(GetUrlKeyValue("SPHostUrl"));    
    var appUrl = decodeURIComponent(GetUrlKeyValue("SPAppWebUrl"));
    

    var scriptBase = hostUrl + "/_layouts/15/";
    $.getScript(scriptBase + "SP.RequestExecutor.js", getItems);
    console.log(scriptBase);

    function getItems() {        

        var executor = new SP.RequestExecutor(appUrl);
        var url = appUrl + "/_api/SP.AppContextSite(@target)/web/lists/getByTitle('Test')/Items?select=Title&@target='" + hostUrl + "'";
       
        executor.executeAsync({
            url: url,
            method: "GET",
            dataType: "json",
            headers: {
                Accept: "application/json;odata=verbose"
            },
            success: function (data) {
                console.log(data.Title);

                var response = JSON.parse(data.body);
                var message = $("#message");
                message.text = ("Test in the host web list");
                message.append("<br />");
                $.each(response.d.results, function (index, value) {
                    
                    message.append(value.Title);
                    message.append("<br />");
                });
            },
            error: function (data, errorCode, errorMessage) {
                alert(errorMessage);
            }
        });
    }
}
