//define GLOBAL variables
var siteUrl = 'https://nsitechnology.sharepoint.com/sites/Engagement/ProjectPortal';

//groupGUID which is needed to read terms from term store
var rxpGroupGuid = "8bf47db1-e07d-4cb9-a4b4-f261ede049c5";
//practiceGUID terms
var rxpPracticeTermsSetGuid = "d8f6e8dd-8c5f-4eec-a948-6bceb7ec42d8";
var rxpProductTermsSetGuid = 'e151c43d-7f4f-4594-813e-24e394f3a462';
var rxpClientTermsSetGuid = 'c596377d-6230-456a-814b-f971776946e7';

var ctxObj = defineContext(siteUrl);

//array to hold Terms ID and names 
var txtClientId = '';
var txtClientName = '';
var txtPracticeId = []
var txtPracticeTerms = []
var txtProductId = []
var txtProductTerms = [];
var practiceTerms = []
var productTerms = []

$(document).ready(function() { 
    //Ensure that all the fields are empty
    allEmpty();
    
    var txtRXPPracticeObj = 'txtRXPPractice';
    var txtRXPProductObj = 'txtRXPProduct';
       
    //Define and set taxonomy picker fields and set their settings
    $('#txtRXPClient').taxpicker({ isMulti: false, allowFillIn: false, useKeywords: true, termSetId: rxpClientTermsSetGuid}, ctxObj)
    $('#txtRXPPractice').taxpicker({ isMulti: true, allowFillIn: false, useKeywords: true, termSetId: rxpPracticeTermsSetGuid}, ctxObj);
    $('#txtRXPProduct').taxpicker({ isMulti: true, allowFillIn: false, useKeywords: true, termSetId: rxpProductTermsSetGuid}, ctxObj);
    
    //give styles to other textfields to match with Metadata textfield
    document.getElementById('txtRXPProjectName').style["width"] = "355px"
    document.getElementById('txtRXPProjectID').style["width"] = "355px"
    document.getElementById('btnCreate').style["margin-left"]= "10px"
    
    $('#txtRXPProjectName').blur(function(){
        var projectName = $('#txtRXPProjectName').val() 
        if(isEmpty(projectName))
        {
            $('#txtRXPProjectNameError').css({'display':'block'})
            $('#txtRXPProjectNameError').val('project name is required')
            $('#txtRXPProjectName').focus() 
        }
        else
        {
            $('#txtRXPProjectNameError').css({'display':'none'})
            $('#txtRXPProjectNameError').val('')
        }   
    });
            
    $('#btnCreate').click(function() {
        
        
        if(requiredFields())
        {           
            //createSubsite();
            insertList();
        }
    });
    
    $('#btnCancel').click(function(){
        
        cancelSite();
    })
});

function insertList() 
{
   
    var txtProjectName = document.getElementById('txtRXPProjectName').value; //get value from project name textfield
    var txtProjectID = document.getElementById('txtRXPProjectID').value; //get value from project Id textfield
    
    var clientObj = jQuery.parseJSON($('#txtRXPClient').val())  
    var practiceObj = jQuery.parseJSON($('#txtRXPPractice').val())  
    var productObj = jQuery.parseJSON($('#txtRXPProduct').val())
    
    var practiceColumnId = 'b5ad2f8a-8367-430c-9f00-904d4a7a5db9';
    var productColumnId = '6649d6af-df35-4725-b591-96ed55419828';  
    
    txtClientId = clientObj[0].Id.toString()    
    txtClientName = clientObj[0].Name.toString()    
    
    for(var i = 0; i < practiceObj.length; i++)
    {
        txtPracticeId.push(practiceObj[i].Id)
        txtPracticeTerms.push(practiceObj[i].Name)
        practiceTerms.push("-1;#" +practiceObj[i].Name+ "|" +practiceObj[i].Id)     
    }

    for(var i = 0; i < productObj.length; i++)
    {
        txtProductId.push(productObj[i].Id)
        txtProductTerms.push(productObj[i].Name)
        productTerms.push("-1;#" +productObj[i].Name+ "|" +productObj[i].Id)
    }
    
    var projectList = ctxObj.get_web().get_lists().getByTitle('Projects');
    
    var clientField =  projectList.get_fields().getByInternalNameOrTitle('Customer');
    var txtClientField = ctxObj.castTo(clientField, SP.Taxonomy.TaxonomyField);
    
    var practiceField = projectList.get_fields().getByInternalNameOrTitle('Practice'); //define Practice Field obj
    var txtPracticeField = ctxObj.castTo(practiceField, SP.Taxonomy.TaxonomyField); //define taxonomy Practice Field obj
        
    var productField = projectList.get_fields().getByInternalNameOrTitle('Product_x0020_or_x0020_Service'); //define Product Field object
    var txtProductField = ctxObj.castTo(productField, SP.Taxonomy.TaxonomyField); //define taxonomy Product Field object
    
    //load all objects
    //load Project List
    ctxObj.load(projectList);
    
    //load Client objects 
    ctxObj.load(clientField);
    ctxObj.load(txtClientField);
    
    //load practice objects
    ctxObj.load(practiceField);
    ctxObj.load(txtPracticeField);    
    
    //load product and service objects
    ctxObj.load(productField);
    ctxObj.load(txtProductField);   
    
    ctxObj.executeQueryAsync(Function.createDelegate(this, function(){          
        
        var itemCreateInfo = new SP.ListItemCreationInformation();
        this.projectListItem = projectList.addItem(itemCreateInfo);
        
        //set value into Project Name column
        projectListItem.set_item('Title', txtProjectName);
        //set value into Project ID column 
        projectListItem.set_item('Project_x0020_ID', txtProjectID); 
        
        //set value for Customer Column
        var termClientField = new SP.Taxonomy.TaxonomyFieldValue()
        termClientField.set_label(txtClientName)
        termClientField.set_termGuid(txtClientId)
        termClientField.set_wssId(-1)
        txtClientField.setFieldValueByValue(projectListItem, termClientField)      
        
        //Insert Metadata Practice multiple value
        var practiceTermsStr = practiceTerms.join(';#')        
        var termValues = new SP.Taxonomy.TaxonomyFieldValueCollection(ctxObj, practiceTermsStr, txtPracticeField);
        txtPracticeField.setFieldValueByValueCollection(projectListItem, termValues)
        
        
        //Insert Metadata Product Multiple value
        var productTermsStr = productTerms.join(';#')     
        var productValues = new SP.Taxonomy.TaxonomyFieldValueCollection(ctxObj, productTermsStr, txtProductField);
        txtProductField.setFieldValueByValueCollection(projectListItem, productValues)
        
        //Process to insert URL field into Project Site column
        var urlValue = new SP.FieldUrlValue();
        urlValue.set_url('https://nsitechnology.sharepoint.com/sites/Engagement/ProjectPortal/' +txtProjectName);
        urlValue.set_description(txtProjectName);
        projectListItem.set_item('Project_x0020_Site', urlValue);
        
        projectListItem.update()
        ctxObj.load(projectListItem)
        
        ctxObj.executeQueryAsync(Function.createDelegate(this, function(){
        
            alert('Failed to insert item: '+args.get_message()+ '\n'+args.get_stackTrace())         
        }), Function.createDelegate(this, function(sender, args){
                    
            alert('Success to insert item')
        }));
        
        
    }), Function.createDelegate(this, function(sender, args){
        alert('Failed to instantiate Fields: ' +args.get_message()+ '\n' +args.get_stackTrace())
    }));
}


function requiredFields() 
{
        var projectName = document.getElementById('txtRXPProjectName').value;
        var clientName = document.getElementById('txtRXPClient').value;
        var practiceName = document.getElementById('txtRXPPractice').value;
        var productName = document.getElementById('txtRXPProduct').value;

        if (isEmpty(projectName)) {
            alert('Project name is required');
            return false;            
        }
        
        if (isEmpty(clientName)) {
            alert('Client name is required');
            return false;            
        }
        
        if(isEmpty(practiceName))
        {
            alert('Practice is required')
            return false;           
        }
        
        if(isEmpty(productName))
        {
            alert('Product / Service is required')
            return false;           
        }
        return true;
}
//isEmpty function. If the field empty, return true else return false.
function isEmpty(strParam) {
    if (strParam.length == 0) {
        return true;
    } else {
        return false;
    }
}

function isNull(strParam)
{
    if(strParam !== null)
    {
        return false
    }
    else
    {
        return true;
    }
}

//defineContext function. Instantiate Context object.
function defineContext(strParam) {

        var ctx = new SP.ClientContext(strParam);
        var result = ctx.executeQueryAsync(Function.createDelegate(this, function() {
                console.log('success define client object');
            }),
            Function.createDelegate(this, function() {
                alert('failed to define object');
            })
        );
        return ctx;
}
//allEmpty function. Ensure that all the fields in the form are empty when the page is loaded
function allEmpty() {
    document.getElementById('txtRXPProjectName').value = '';
    document.getElementById('txtRXPProjectID').value = '';
    document.getElementById('txtRXPClient').value = '';
    document.getElementById('txtRXPPractice').value = '';
    document.getElementById('txtRXPProduct').value = '';
}

//CancelSite function. Navigate user back to Project Portal site
function cancelSite() {
    var ctx = new SP.ClientContext.get_current();
    var result = ctx.executeQueryAsync(Function.createDelegate(this, function() {
            window.location.href = "/Sites/Engagement/ProjectPortal";
        }),
        Function.createDelegate(this, function() {
            console.log('Failed to navigate to portal site');
        })
    );
}

//Function to create subsite
function createSubsite() {
    var siteName = $('#txtRXPProjectName').val();
    
    //define variables to hold new subsite properties       
    var webDescription = 'A new site for project' + siteName;
    var webLanguage = 1033;
    var webTitle = siteName;
    var webUrl = siteName;
    var webPermissions = true;
    // Just insert your reference to your feature and webtemplate here
    var webTemplate = 'STS#0';

    //get the SharePoint Web object
    var rxpWeb = ctxObj.get_web();

    //Assign all the new subsite properties
    var webCreateInfo = new SP.WebCreationInformation();
    webCreateInfo.set_description(webDescription);
    webCreateInfo.set_language(webLanguage);
    webCreateInfo.set_title(webTitle);
    webCreateInfo.set_url(webUrl);
    webCreateInfo.set_useSamePermissionsAsParentSite(webPermissions);
    webCreateInfo.set_webTemplate(webTemplate);

    //Adding new subsite and attach into SharePoint Web Object
    rxpWeb = rxpWeb.get_webs().add(webCreateInfo);
    ctxObj.load(rxpWeb);

    ctxObj.executeQueryAsync(Function.createDelegate(this, function(sender, args) {
            alert('Failed to create site' + '\n' + 'request failed ' + args.get_message() + '\n' + args.get_stackTrace());
        }),
        Function.createDelegate(this, function() {
            
            alert('success to create site: ' + siteName);//when sucess
        })
    );
}
