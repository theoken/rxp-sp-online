﻿if ((Get-Module Microsoft.Online.SharePoint.PowerShell).Count -eq 0) 
{
    Import-Module Microsoft.Online.SharePoint.PowerShell -DisableNameChecking
}

Set-ExecutionPolicy bypass -Force 

#array to hold DLL files path
[string []] $files = @("C:\Program Files\Common Files\microsoft shared\Web Server Extensions\16\ISAPI\Microsoft.SharePoint.Client.dll",
            "C:\Program Files\Common Files\microsoft shared\Web Server Extensions\16\ISAPI\Microsoft.SharePoint.Client.Runtime.dll",
            "C:\Program Files\Common Files\microsoft shared\Web Server Extensions\16\ISAPI\Microsoft.SharePoint.Client.Taxonomy.dll"           
            );

#iterate the DLL and check if the files are exist
foreach($file in $files)
{
    Get-ChildItem $file
    
    if(Test-Path $file)
    {
        Add-Type -Path $file
    }
    else
    {
        write-host "file is not exist. Download the DLL files and put into the C:\Program Files\Common Files\microsoft shared\Web Server Extensions\15\ISAPI folder"
    }   
}

# Insert the credentials and the name of the admin site
$Username="kenny.soetjipto@rxpservices.com"
$AdminPassword=Read-Host -Prompt "Password" -AsSecureString
$AdminUrl="https://nsitechnology.sharepoint.com/sites/contentTypeHub"
$ParentContentTypeID="0x01"

#Define Client object with adminUrl -> ContentTypeHub site
$ctx=New-Object Microsoft.SharePoint.Client.ClientContext($AdminUrl)
$ctx.Credentials = New-Object Microsoft.SharePoint.Client.SharePointOnlineCredentials($Username, $AdminPassword)
$ctx.RequestTimeout = 7200000; 
$ctx.ExecuteQuery()

write-host "Initialize global variables" -BackgroundColor Cyan -ForegroundColor Black
$gWeb = $ctx.Web;
$ctx.Load($gWeb)
$ctx.ExecuteQuery()
write-host $gWeb.Title " has been initialized"
$gList = $ctx.Web.Lists;
$ctx.ExecuteQuery()
Write-Host $gList.ToString() " has been initialized"
$gField = $ctx.Web.Fields;
$ctx.Load($gField)
$ctx.ExecuteQuery()
write-host $gField

#RXP root Variables declarations
$rootRXPParent = "Document";
$rootRXPCT = "RXP Document";
$RootRXPDescription = "A single parent for all RXP content types";

#RXP first level variable declarations
$rxpFirstCTs = "Contract Document", "Meeting Document", "Supporting Document", "Project Document", "Financial Document", "Email", "Reference Document", "Marketing Document"
$rxpFirstCTDescriptions = "A wrapper for all contract documents to provide common metadata", 
                            "A wrapper for all meeting documents to provide common metadata",
                            "A wrapper for all supporting documents to provide common metadata",
                            "A wrapper for all project docucments to provide common metadata", 
                            "A wrapper for all financial documents to provide common metadata",
                            "A wrapper for all Email documents to provide common metadata",
                            "External reference documents used for research purposes or as supporting evidences",
                            "Common parent for all marketing & collateral documents to manage common metadata"

#Create Contract sub content types
$rxpContractCTParent = "Contract Document";
$rxpContractCTs = @("Contract", "Variation");
$rxpContractCTDescriptions = @("Any RXP contract document related to project", 
                               "Variations");

#Create Meeting sub content types
$rxpMeetingCTParent = "Meeting Document";
$rxpMeetingCTs = @("Agenda", "Minutes")
$rxpMeetingCTDescriptions = @("RXP agenda associate with meeting", 
                              "RXP minutes associate with meeting");

#Create Project sub content types
$rxpProjectsCTParent = "Project Document"
$rxpProjectsCTs = @("Project Charter", "Deliverable", "Statement of Work", "Proposal", 
                    "Purchase Order", "Project Schedule", "Project Report", "Client Document");

$rxpProjectsCTsDescriptions = @("Project charter for RXP organisations",
                                "RXP Deliverable", 
                                "RXP statement of work", 
                                "RXP proposal",
                                "RXP purchase order",
                                "RXP project schedule",
                                "A wrapper for all project report documents to provide common metadata", 
                                "Any client documents which associate certain project");

#Create Project report sub content types
$rxpProjectReportParent = "Project Report"
$rxpProjectReportCTs = @("Status Report", "QA Report", "Closure Report")
$rxpProjectReportCTsDescriptions = @("Status report of RXP projects.",
                                     "RXP QA report.",
                                     "RXP closure report.");
#Create Marketing sub content type
$rxpMarketingCTParent = "Marketing Document"
$rxpMarketingCTs = @("Corporate Collateral",
                     "Case Study",
                     "White Paper",
                     "Consultant Profile",
                     "RXP Template");

$rxpMarketingCTsDescriptions = @("Core corporate collateral", 
                                 "RXP Case Studies of previous engagements designed to be used in pre-sales activity with potential clients",
                                 "White Papers written by RXP that can be used to promote the company’s expertise",
                                 "Consultant profiles maintained to be provided to clients as part of pre-sales activity",
                                 "Branded document templates which will be used by consultants");

$rxpVideoCTParent = "Video"
$rxpVideoCT = "RXP Video"
$rxpVideoCTDescription = "RXP produced videos"

$rxpImageCTParent = "Image"
$rxpImageCT = "RXP Image"
$rxpImageCTDescriptions = "RXP images and logos"

$rxpPersonnelCTParent = "RXP Document"
$rxpPersonnelDocument = "Personnel Document"
$rxpPersonnelDocumentDescription = "Generic parent content type for HR-related documents"

$rxpEmploymentContractParent = "Personnel Document"
$rxpEmploymentContract = "Employment Contract"
$rxpEmploymentContractDescription = "Content type for all RXP personnel employment contracts"

#--------------------------------------------------------------------------------------------------------------------------------------
#DOCUMENT SET content type Object declarations
#Is used to define DOCUMENT SET content type
#--------------------------------------------------------------------------------------------------------------------------------------

#Define RXP Document Set
$rxpDocumentSetParent = "Document Set"
$rxpDocumentSet = "RXP Document Set"
$rxpDocumentSetDescription = "A single parent for all RXP document set content types. Not intended to be used directly"

#Define Contract Document Set
$rxpContractDocumentSetParent = "RXP Document Set"
$rxpContractDocumentSet = "Contract Document Set"
$rxpContractDocumentSetDescription = "A common document set for managing all type of RXP contracts"

#Define Meeting Document Set
$rxpMeetingDocumentSetParent = "RXP Document Set"
$rxpMeetingDocumentSet = "Meeting Document Set"
$rxpMeetingDocumentSetDescription = "A common document set for managing all types of RXP meeting"

#Create Personnel record Set
$rxpPersonnelRecordSetParent = "RXP Document Set"
$rxpPersonnelRecordSet = "Personnel Record"
$rxpPersonnelRecordSetDescription = "A container for all personnel files regarding a specific employee."

#Create Marketing Brief Request Content Type
$rxpMarketingBriefCTParent = "Item"
$rxpMarketingBriefCT = "Marketing Brief Request"
$rxpMarketingBriefCTDescription = "Custom list item for managing consultant requests for marketing briefs"

#--------------------------------------------------------------------------------------------------------------------------------------
#Create RXP Corporate CT
#--------------------------------------------------------------------------------------------------------------------------------------
#Policy
$rxpCorpCTParent = "RXP Document"
$rxpCorpPolicyCT = "Policy"
$rxpCorpPolicyDesc = "A container for all RXP policies files"

#Strategy
$rxpCorpStrategyCTParent = "RXP Document"
$rxpCorpStrategyCT = "Strategy"
$rxpCorpStrategyDesc = "A container for all RXP Strategy files"

#Annual Report                                 
$rxpCorpAnnualCTParent = "RXP Document"
$rxpCorpAnnualCT = "Annual Report"
$rxpCorpAnnualCTDesc = "A container for RXP Annual report files"