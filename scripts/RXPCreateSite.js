//define GLOBAL variables
var siteUrl = 'https://nsitechnology.sharepoint.com/sites/Engagement/ProjectPortal';
//var subSiteUrl = 'https://nsitechnology.sharepoint.com/sites/Engagement/ProjectPortal/2016-ANZ-KenTest10';
var subSiteUrl = '';
//Invoke checkUser function. 

//groupGUID which is needed to read terms from term store
const rxpGroupGuid = "8bf47db1-e07d-4cb9-a4b4-f261ede049c5";
//practiceGUID terms
const rxpPracticeTermsSetGuid = "d8f6e8dd-8c5f-4eec-a948-6bceb7ec42d8";
const rxpProductTermsSetGuid = 'e151c43d-7f4f-4594-813e-24e394f3a462';
const rxpClientTermsSetGuid = 'c596377d-6230-456a-814b-f971776946e7';

//global variables
const ctxObj = defineContext(siteUrl);
//var subCtx = defineContext(subSiteUrl);

//is used to define context each time subsite is created
var subCtx = {}; 

const groupName = 'Engagement Owners';
const LISTLIBRARIES = ['Commercials', 'Project Management', 'Deliverables', 'Reports', 'External Documents'];
const PROJECTFIELD = 'Project_x0020_Name';
const CUSTOMERFIELD = 'Customer';
const PROJECTFIELDVALUE = 'project name';

//for testing development only
const PROJECTDELETEFIELDVALUE = '';
const CUSTOMERDELETEFIELDVALUE = '';

// Just insert your reference to your feature and webtemplate here.
//webTemplate value is retrieved from powershell script - GetSiteTemplate() function
const WEBTEMPLATE = '{528C205D-9BCB-47F4-8F58-76978B800911}#Project Site Template';
var defer = $.Deferred(); //promise in JQuery

//array to hold Terms ID and names 
var txtClientId = '';
var txtClientName = '';
var txtPracticeId = [];
var txtPracticeTerms = [];
var txtProductId = [];
var txtProductTerms = [];
var practiceTerms = [];
var productTerms = [];

//Authenticate users when the body page is loaded
ExecuteOrDelayUntilBodyLoaded(checkUser);

$(document).ready(function() {
        
    //checkUser(); 
    //Ensure that all the fields are empty when the page is loaded
    allEmpty();
    console.log('Test1');
    
    //Register Project which create subsite and insert items into Project list  
    //Function below is ensure that necessary scrips file is loaded before execute registerProject function
    ExecuteOrDelayUntilScriptLoaded(registerProject,'sp.js');
    console.log(subCtx);
   
});

function registerProject()
{
    var txtRXPPracticeObj = 'txtRXPPractice';
    var txtRXPProductObj = 'txtRXPProduct';
    
    //Define and set taxonomy picker fields and set their settings
    $('#txtRXPClient').taxpicker({ isMulti: false, allowFillIn: false,useHashtags:true, useKeywords: true, termSetId: rxpClientTermsSetGuid}, ctxObj);
    $('#txtRXPPractice').taxpicker({ isMulti: true, allowFillIn: false, useKeywords: true, termSetId: rxpPracticeTermsSetGuid}, ctxObj);
    $('#txtRXPProduct').taxpicker({ isMulti: true, allowFillIn: false, useKeywords: true, termSetId: rxpProductTermsSetGuid}, ctxObj);
    
    //give styles to other textfields to match with Metadata textfield
    document.getElementById('txtRXPProjectName').style["width"] = "355px";
    document.getElementById('txtRXPProjectID').style["width"] = "355px";
    document.getElementById('btnCreate').style["margin-left"]= "10px";
    
    //give styles to suggestion container
    document.getElementsByClassName('cam-taxpicker-suggestion-container')[0].style.marginTop = "25px";
    document.getElementsByClassName('cam-taxpicker-suggestion-container')[1].style.marginTop = "25px";

    //give class attribute for client textfield div auto generate
    var clientField = document.getElementsByClassName('cam-taxpicker')[0];
    clientField.setAttribute("class", "rxpClientField");
    
    var practiceField = document.getElementsByClassName('cam-taxpicker')[0];
    practiceField.setAttribute("class", "rxpPracticeField");
        
    //Project Name textfield lost focus function
    $('#txtRXPProjectName').blur(function(){
        
        var projectName = document.getElementById('txtRXPProjectName').value;
        
        if(!isEmpty(projectName))
        {
            document.getElementById('txtRXPProjectNameError').style.display = 'none';
        }
        else
        {
            document.getElementById('txtRXPProjectNameError').style.display = 'block';
            document.getElementById('txtRXPProjectNameError').value = "Project name is required";
            document.getElementById('txtRXPProjectName').focus();
        }
    });   

    $('.rxpClientField .cam-taxpicker-editor').blur(function(){
        
        var clientName = document.getElementById('txtRXPClient').value;
        var divClientName = $('.rxpClientField .cam-taxpicker-editor')[0];
        
        //Need to validate 2 fields. One is hidden field, and auto-generate div tag             
        if(clientName.length != 2 && divClientName.childNodes.length != 1)//if there is a value
        {           
            document.getElementById('txtRXPClientError').style.display = 'none';
        }
        else
        {           
            document.getElementById('txtRXPClientError').style.display = 'block';
            document.getElementById('txtRXPClientError').value = 'Client name is required';
        }
    });    

    $('.rxpPracticeField .cam-taxpicker-editor').blur(function(){
        
        var practiceName = document.getElementById('txtRXPPractice').value;
        var divPracticeName = $('.rxpPracticeField .cam-taxpicker-editor')[0];
                
        //Need to validate 2 fields. One is hidden field, and auto-generate div tag
        if(practiceName.length != 2 && divPracticeName.childNodes.length != 1) //if there is a value
        {
            document.getElementById('txtRXPPracticeError').style.display = 'none';
        }
        else
        {
            document.getElementById('txtRXPPracticeError').style.display = 'block';
            document.getElementById('txtRXPPracticeError').value = "Practice is required";
        }
    }); 
        
    $('#btnCreate').click(function(e) {
        
        e.preventDefault();
        
        console.log('create button clicked 1');
        //variables to update Managed Metadata Default value
        var clientObj = jQuery.parseJSON($('#txtRXPClient').val());
        var mmClientValue = `-1;#${clientObj[0].Name.toString()}|${clientObj[0].Id}`;

        var projectsObj = [];
        var customersObj = [];

        $('.spinner').show();


        createSubsite()
        .then((res) => {
            console.log('success to create subsite');
            return defineSubContext(subSiteUrl);
        })
        .then(() => {
            console.log('Success to define subContext');
            return createGroup();
        })
        .then(() => {
            console.log('Success to create group');
            return updateGroup();
        })        
        .then(() => {
            console.log('Success to update group');
            return renameOneNote('Site Assets');            
        })
        .then((oneNoteRes) => {
            console.log('Success to retrieve oneNote object');
            return enumerateItems(oneNoteRes);
        })
        .then(() => {
             console.log('Success to rename oneNote project')
            return topNavigationSetting();
        })
        .then(() => {
            console.log('Success to change top navigation setting');
            return insertList(); 
        })
        .then((projectListRes) => {
            console.log('Success to instantiate Project List');
            return insertProjectItem(projectListRes);
        })
       .then(() => {
            console.log('Success insert item into Project List');

            for(let i = 0; i < LISTLIBRARIES.length; i++) {
                updateDefaultField(LISTLIBRARIES[i], PROJECTFIELD, PROJECTFIELDVALUE);
            }
        })
        .then(() => {
            console.log('Success to add defaultValue for Project Fields');

            for(let a = 0; a < LISTLIBRARIES.length; a ++) {
                updateDefaultField(LISTLIBRARIES[a], CUSTOMERFIELD, mmClientValue);
            }
        })        
        .then(() => {
            console.log('Success to add defaultValue for Customer Fields');
            return navigateTo();
        })
        .then(() => {
            console.log('Success to navigate to the site');
        })        
        .catch(()=> {
            console.log('There is error in chain');
        }); 
    });
    
    $('#btnCancel').click(function(){
        cancelSite();
    });
}




function updateDefaultField(listNameParam, fieldNameParam, fieldValueParam) {

    getList(listNameParam)
    .then((listRes) => {
        return getField(listRes, fieldNameParam, fieldValueParam);
    })
    .then((fieldRes) => {
        console.log('success update fields');
    })
    .catch((e) => {
        console.log('Error occur');
        console.log(e);
    })
}

//Use this function for development purposes only
function getDeleteField(listObjParam, fieldObjParam, fieldValue){
    
    return new Promise((resolve, reject) => {

        let fieldObj = listObjParam.get_fields().getByInternalNameOrTitle(fieldObjParam);
        fieldObj.set_defaultValue(fieldValue);
        fieldObj.update();

        subCtx.load(fieldObj);

        subCtx.executeQueryAsync(Function.createDelegate(this, (sender, args) => {
            console.log('Success to get Field ', fieldObj.get_defaultValue());       
            
            resolve(fieldObj);

            }), Function.createDelegate(this, (sender, args) => {
        
                console.log('Failed in Get Field by title ' +args.get_message());
                reject('Error ', +args);
            }));
    });

}



//validate all the fields in the form whether empty or not
function requiredFields() 
{
        var projectName = document.getElementById('txtRXPProjectName').value;
        //var clientObj = jQuery.parseJSON($('#txtRXPClient').val())
        var clientName = document.getElementById('txtRXPClient').value;
        //var productObj = jQuery.parseJSON($('#txtRXPProduct').val())
        var practiceName = document.getElementById('txtRXPPractice').value;

        //validation on ProjectName field
        if (isEmpty(projectName)) {
            document.getElementById('txtRXPProjectNameError').style.display = 'block';
            document.getElementById('txtRXPProjectNameError').value = "Project name is required";
            return false;            
        }
        else
        {
            document.getElementById('txtRXPProjectNameError').style.display = 'none';
        }
        
        //validation on ClientName field
        if (isEmpty(clientName)) {
            document.getElementById('txtRXPClientError').style.display = 'block';
            document.getElementById('txtRXPClientError').value = "Client name is required";
            return false;            
        }
        else
        {
            document.getElementById('txtRXPClientError').style.display = 'none';
        }      
        
        //validation on practice field
        if(isEmpty(practiceName))
        {
            document.getElementById('txtRXPPracticeError').style.display = 'block';
            document.getElementById('txtRXPPracticeError').value = "Practice is required";
            return false;           
        }
        else
        {
            document.getElementById('txtRXPPracticeError').style.display = 'none';
        }
        
        //if all the fields have correct value then return true        
        return true;
}
//check user when the page is loaded whether the user has priviledge to access the page
function checkUser()
{

        isCurrentUserEngagementOwner(groupName, function(isCurrentUserInGroup)
        {
            if(isCurrentUserInGroup)//if the Engagement Owner, show the register project
            {
                document.getElementById('RXPHeading').style.display = 'block';
                document.getElementsByClassName('tblCreateSite')[0].style.display = 'block';
                document.getElementById('WebPartWPQ2').style.display = 'none';
            }
            else //if the Engagement Member, show the Access Denied
            {
                document.getElementsByClassName('tblCreateSite')[0].style.display = 'none';
                document.getElementById('RXPHeading').style.display = 'none';
                document.getElementsByClassName('rxpAccessDenied')[0].style.display = 'block';
            }
        });
}
//check whether the user is in EngagementOwner group
function isCurrentUserEngagementOwner(groupName, OnComplete)
{
    var currentWeb = ctxObj.get_web();
    
    //get currentUser object
    var currentUser = ctxObj.get_web().get_currentUser();
    ctxObj.load(currentUser);
    
    //get All groups available in current web
    var allGroups = currentWeb.get_siteGroups();
    ctxObj.load(allGroups);
    
    //get group object of EngagementOwner
    var group = allGroups.getByName(groupName); 
    ctxObj.load(group);
    
    var groupUsers = group.get_users();
    ctxObj.load(groupUsers);
    
    ctxObj.executeQueryAsync(function(sender,args){
        var userInGroup = isUserInGroup(currentUser, group);
        OnComplete(userInGroup);
    },
    function OnFailure(sender, args){
        OnComplete(false);
    }
    );
    
    //function to check if user in certain usergroup. Return boolean
    function isUserInGroup(user, group)
    {
        var groupUsers = group.get_users();
        var userInGroup = false;
        var groupUserEnumerator = groupUsers.getEnumerator();
        while(groupUserEnumerator.moveNext())
        {
            var groupUser = groupUserEnumerator.get_current();
            if(groupUser.get_id() == user.get_id())
            {
                userInGroup = true;
                break;
            }
        }
        return userInGroup;
    }
}
//to show all groups - development purposes
function showAllGroups()
{
    var groupColl = ctxObj.get_web().get_siteGroups();
    ctxObj.load(groupColl);

    ctxObj.executeQueryAsync(Function.createDelegate(this, function(){
                
        var groupEnum = groupColl.getEnumerator();
        
        while(groupEnum.moveNext())
        {
            var groupItem = groupEnum.get_current();
            var groupName = groupItem.get_title();
            console.log(groupName);
        }
    
    }), Function.createDelegate(this, function(sender, args){
        alert('Failed: ' +args.get_message()+ '\n'+args.get_stackTrace());
    }));
    
}



//to show subGroups in subsite
function displaySubGroup() {
    
    //get all groups in subsite
    var groupObj = subCtx.get_web().get_siteGroups();

    subCtx.load(groupObj);
    
    subCtx.executeQueryAsync(Function.createDelegate(this, (sender, args) =>{
        console.log('inside success display subGroup');
        defer.resolve(groupObj);

    }), Function.createDelegate(this, (sender, args) => {
        console.log('Failed to display subGroup');
        defer.reject(args);
    }));

    return defer.promise();   
}


//get all Lists 
function getAllLists() {
    let listsObj = subCtx.get_web().get_lists();

    subCtx.load(listsObj);
   
    subCtx.executeQueryAsync(Function.createDelegate(this, (sender, args) => {
        
        defer.resolve(listsObj);

    }), Function.createDelegate(this, (sender, args) => {
        console.log('Failed in GetAllLists');
        defer.reject('Error ', +args)
    }));

    return defer.promise();
}


//isEmpty function. If the field empty, return true else return false.
function isEmpty(strParam) {
    if (strParam.length == 0) {
        return true;
    } else {
        return false;
    }
}



//allEmpty function. Ensure that all the fields in the form are empty when the page is loaded
function allEmpty() {
    document.getElementById('txtRXPProjectName').value = '';
    document.getElementById('txtRXPProjectID').value = '';
    document.getElementById('txtRXPClient').value = '';
    document.getElementById('txtRXPPractice').value = '';
    document.getElementById('txtRXPProduct').value = '';
}

function navigateTo() {
    return new Promise((resolve, reject) => {
        var ctx = new SP.ClientContext.get_current();

        var result = ctx.executeQueryAsync(Function.createDelegate(this, function(sender, args) {
                window.location.href = subSiteUrl;
                resolve(ctx);
            }),
            Function.createDelegate(this, function(sender, args) {
                console.log('Failed to navigate to the site');
                reject('Error ', args);
            })
        );
    })
}

//CancelSite function. Navigate user back to Project Portal site
function cancelSite() {
    var ctx = new SP.ClientContext.get_current();
    var result = ctx.executeQueryAsync(Function.createDelegate(this, function() {
            window.location.href = "/Sites/Engagement/ProjectPortal";
        }),
        Function.createDelegate(this, function() {
            console.log('Failed to navigate to portal site');
        })
    );
}


