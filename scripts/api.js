//function to insert item into Project list
function insertList() 
{    
    return new Promise((resolve, reject) => {
    
        var projectList = ctxObj.get_web().get_lists().getByTitle('Projects');
        
        ctxObj.load(projectList);

        ctxObj.executeQueryAsync(Function.createDelegate(this, function(){          
            console.log('success to instantiate Project List')
            resolve(projectList);
        }), Function.createDelegate(this, function(sender, args){
            console.log('failed to instantiate fields')
            //alert('Failed to instantiate Fields: ' +args.get_message()+ '\n' +args.get_stackTrace());
            reject('Error ', args);
        }));   
    });//end of return Promise
}

function insertProjectItem(projectList) {

    return new Promise((resolve, reject) => {
        
        var txtProjectName = document.getElementById('txtRXPProjectName').value; //get value from project name textfield
        var txtProjectID = document.getElementById('txtRXPProjectID').value; //get value from project Id textfield      
        
        var practiceColumnId = 'b5ad2f8a-8367-430c-9f00-904d4a7a5db9';
        var productColumnId = '6649d6af-df35-4725-b591-96ed55419828';  
        
        //variables to hold parsing JSON value
        var clientObj = jQuery.parseJSON($('#txtRXPClient').val());          
        var practiceObj = jQuery.parseJSON($('#txtRXPPractice').val());
        var productObj;
        
        var clientField =  projectList.get_fields().getByInternalNameOrTitle('Customer');
        var txtClientField = ctxObj.castTo(clientField, SP.Taxonomy.TaxonomyField);
    
        var practiceField = projectList.get_fields().getByInternalNameOrTitle('Practice'); //define Practice Field obj
        var txtPracticeField = ctxObj.castTo(practiceField, SP.Taxonomy.TaxonomyField); //define taxonomy Practice Field obj
        
        var productField = projectList.get_fields().getByInternalNameOrTitle('Product_x0020_or_x0020_Service'); //define Product Field object
        var txtProductField = ctxObj.castTo(productField, SP.Taxonomy.TaxonomyField); //define taxonomy Product Field object

        try
        {
            productObj = jQuery.parseJSON($('#txtRXPProduct').val());
        }
        catch(e)
        {
            productObj = "";
        }

        txtClientId = clientObj[0].Id.toString();    
        txtClientName = clientObj[0].Name.toString();    

        //Ensure that productObj has value, if there is, perform operation below
        if(productObj.length > 0)
        {
            for(var i = 0; i < productObj.length; i++)
            {
                txtProductId.push(productObj[i].Id);
                txtProductTerms.push(productObj[i].Name);
                productTerms.push("-1;#" +productObj[i].Name+ "|" +productObj[i].Id);
            }   
        }
    
        for(var i = 0; i < practiceObj.length; i++)
        {
            txtPracticeId.push(practiceObj[i].Id);
            txtPracticeTerms.push(practiceObj[i].Name);
            practiceTerms.push("-1;#" +practiceObj[i].Name+ "|" +practiceObj[i].Id);     
        }                

        var itemCreateInfo = new SP.ListItemCreationInformation();        
        var projectListItem = projectList.addItem(itemCreateInfo);        
        ctxObj.load(projectListItem);
        
        //set value into Project Name column
        projectListItem.set_item('Title', txtProjectName);
        //set value into Project ID column 
        projectListItem.set_item('Project_x0020_ID', txtProjectID); 
        
        //load Client objects 
        ctxObj.load(clientField);
        ctxObj.load(txtClientField);
        
        //load practice objects
        ctxObj.load(practiceField);
        ctxObj.load(txtPracticeField);    
        
        //load product and service objects
        ctxObj.load(productField);
        ctxObj.load(txtProductField);

        //set value for Customer Column
        var termClientField = new SP.Taxonomy.TaxonomyFieldValue();
        termClientField.set_label(txtClientName);
        termClientField.set_termGuid(txtClientId);
        termClientField.set_wssId(-1);
        txtClientField.setFieldValueByValue(projectListItem, termClientField);      
        
        //Insert Metadata Practice multiple value       
        var practiceTermsStr = practiceTerms.join(';#');        
        var termValues = new SP.Taxonomy.TaxonomyFieldValueCollection(ctxObj, practiceTermsStr, txtPracticeField);
        txtPracticeField.setFieldValueByValueCollection(projectListItem, termValues);
                
        
        //Insert Metadata Product Multiple value
        if(productObj.length > 0)
        {
            var productTermsStr = productTerms.join(';#');    
            var productValues = new SP.Taxonomy.TaxonomyFieldValueCollection(ctxObj, productTermsStr, txtProductField);
            txtProductField.setFieldValueByValueCollection(projectListItem, productValues);
        }       
        

        //Process to insert URL field into Project Site column
        var urlValue = new SP.FieldUrlValue();
        urlValue.set_url('https://nsitechnology.sharepoint.com/sites/Engagement/ProjectPortal/' +txtProjectName);
        urlValue.set_description(txtProjectName);
        projectListItem.set_item('Project_x0020_Site', urlValue);
        
        projectListItem.update();
        ctxObj.load(projectListItem);
        
        

        ctxObj.executeQueryAsync(Function.createDelegate(this, function(sender, args){          
           
            resolve(projectListItem);    
            
        }), Function.createDelegate(this, function(sender, args){
            
            //alert('Success to insert item');
            reject('Error in ListItem ', args);  
        }));

    }); //End of Promise
        
}


function topNavigationSetting() {
    
    return new Promise((resolve, reject)=>{

        var scriptBase = _spPageContextInfo.webAbsoluteUrl+ '/_layouts/15/';
        
        $.getScript(scriptBase+ 'sp.publishing.js', () => {
            var webObj = subCtx.get_web();

                var webNavSetting = new SP.Publishing.Navigation.WebNavigationSettings(subCtx, webObj);
                var navigation = webNavSetting.get_globalNavigation();
                
                navigation.set_source(3);
                webNavSetting.update();

                subCtx.load(webNavSetting);

                subCtx.executeQueryAsync(Function.createDelegate(this, (sender, args) => {
                    console.log('Success to change top navigation setting ', webNavSetting);
                    resolve(webNavSetting);
                }), Function.createDelegate(this, (sender, args) => {
                    reject('Error ', args);
                }));
        });

    });
}

//renameOneNote 
function renameOneNote(listTitle) {
    
    return new Promise((resolve, reject) => {
         
         var clientObj = subCtx.get_web().get_lists().getByTitle(listTitle);
         var query = new SP.CamlQuery.createAllItemsQuery();
         
         var queryResult = clientObj.getItems(query);
         
         subCtx.load(queryResult);

          subCtx.executeQueryAsync(Function.createDelegate(this, (sender, args) => {
            console.log('success to get list ', queryResult);
            resolve(queryResult);

        }), Function.createDelegate(this, (sender, args) => {
            
            console.log('Failed in Get List by title');
            reject('Error ', +args);
        }));
    })
}

//is used to rename oneNote document library
function enumerateItems(itemObj) {
    
    return new Promise((resolve, reject) => {
        //get value from project textfield and it is used as name of oneNote
        var projectName = document.getElementById('txtRXPProjectName').value;
        
        var itemEnum = itemObj.getEnumerator();

            while(itemEnum.moveNext()) {
                var currentItem = itemEnum.get_current();
                
                //console.log(currentItem.get_item('Title')+ ' and ID: ' +currentItem.get_id());

                if(currentItem.get_item('Title') === 'Project Notebook') {
                    
                    currentItem.set_item('Title', projectName); //Title - renamte in Title column - required
                    currentItem.set_item('FileLeafRef', projectName); //FileLeafRef - rename in Name column, required
                    
                    currentItem.update();
                    subCtx.load(currentItem);
                    
                    subCtx.executeQueryAsync(Function.createDelegate(this, (sender, args) => {
                        console.log('success to update item ', currentItem);
                        resolve(currentItem);                        

                    }), Function.createDelegate(this, (sender, args) => {
                        console.log('Failed to update title');
                        reject('Error ', args);
                        
                    }));
                }
               //console.log(currentItem.get_item('Title')+ ' and ID: ' +currentItem.get_id());
            }
    });
}

function getField(listObjParam, fieldObjParam, fieldValue){
    
    return new Promise((resolve, reject) => {

        let fieldObj = listObjParam.get_fields().getByInternalNameOrTitle(fieldObjParam);
        fieldObj.set_defaultValue(fieldValue);
        fieldObj.update();

        subCtx.load(fieldObj);

        subCtx.executeQueryAsync(Function.createDelegate(this, (sender, args) => {
            console.log('Success to get Field ', fieldObj.get_defaultValue());       
            
            resolve(fieldObj);

            }), Function.createDelegate(this, (sender, args) => {
        
                console.log('Failed in Get Field by title');
                reject('Error ', +args);

            }));
    });

}

function getList( title) {

    return new Promise((resolve, reject) => {
        var listsObj = subCtx.get_web().get_lists().getByTitle(title);
        subCtx.load(listsObj);
    
        subCtx.executeQueryAsync(Function.createDelegate(this, (sender, args) => {
            console.log('success to get list ', listsObj.get_title());
            resolve(listsObj);

        }), Function.createDelegate(this, (sender, args) => {
            
            console.log('Failed in Get List by title');
            reject('Error ', +args);
        }));
    });
}

//Promise Function to create subsite - return getWeb() 
function createSubsite() {
    
    var siteName = $('#txtRXPProjectName').val();
    var siteYear = new Date().getFullYear();
    var clientObj = jQuery.parseJSON($('#txtRXPClient').val()); 
    var txtClientName = clientObj[0].Name.toString();
    var txtProjectName = document.getElementById('txtRXPProjectName').value; 
    
    return new Promise((resolve, reject) => {
        

        let rxpSiteNameUrl = `${siteYear}-${txtClientName}-${txtProjectName}`;

        //define variables to hold new subsite properties       
        var webDescription = 'A new site for project' + siteName;
        var webLanguage = 1033;
        var webTitle = siteName;
        var webUrl = rxpSiteNameUrl;

        //As Create Project Site instruction, it requires to use unique permission
        var webPermissions = false;

        //get the SharePoint Web object
        var rxpWeb = ctxObj.get_web();
        

         //Assign all the new subsite properties
        var webCreateInfo = new SP.WebCreationInformation();
        webCreateInfo.set_description(webDescription);
        webCreateInfo.set_language(webLanguage);
        webCreateInfo.set_title(webTitle);
        webCreateInfo.set_url(webUrl);

        //define new clientContext obj for subsite
        subSiteUrl = `${siteUrl}/${webUrl}`;    

        //define to use permission as parent site - false
        webCreateInfo.set_useSamePermissionsAsParentSite(webPermissions);

        //define webTemplate to use - Project Site Template above
        webCreateInfo.set_webTemplate(WEBTEMPLATE);

        //Adding new subsite and attach into SharePoint Web Object
        rxpWeb = rxpWeb.get_webs().add(webCreateInfo);

        ctxObj.load(rxpWeb);

        ctxObj.executeQueryAsync(Function.createDelegate(this, function(sender, args) {                
                console.log('Success to create subsite ', rxpWeb);
                resolve(rxpWeb);
            }),
            Function.createDelegate(this, function(sender, args) {                     
                console.log('Failed to create subsite ');
                defer.reject(args);
            })
        );
    });
   
}
//create group for the new site. It requires to give siteName
function createGroup() { 

    //get siteName project
    let siteName = $('#txtRXPProjectName').val();
    
    //define group names
    let groupSiteMember = `${siteName} Members`;
    let groupSiteOwners = `${siteName} Owners`;

   
    return new Promise((resolve, reject) => {
        
        //create subsite memberObj group 
        let groupSiteMemberObj = new SP.GroupCreationInformation();
        groupSiteMemberObj.set_title(groupSiteMember);
        groupSiteMemberObj.set_description('Use this group to grant people - ' +groupSiteMember+ ' contribute permission to SharePoint Site: ');
        
        //create subsite ownerObj group
        let groupSiteOwnerObj = new SP.GroupCreationInformation();
        groupSiteOwnerObj.set_title(groupSiteOwners);
        groupSiteOwnerObj.set_description('Use this group to grant people - ' +groupSiteOwners+ ' Full Control permission to SharePoint Site: ');
        
        //add ownerObj and memberObj group
        let subWebGroup = subCtx.get_web().get_siteGroups().add(groupSiteMemberObj);
        subWebGroup.set_onlyAllowMembersViewMembership(false); //everyone can see member group
        subWebGroup.update(); //needed to update setting - false above. 

        let subWebOwnerGroup = subCtx.get_web().get_siteGroups().add(groupSiteOwnerObj);
        subWebOwnerGroup.set_onlyAllowMembersViewMembership(false); //everyone can see owner group
        subWebOwnerGroup.update(); //needed to update setting - false above. 

        //get role definition by name - Contribute
        var rdContribute = subCtx.get_web().get_roleDefinitions().getByName('Contribute');
        //get role definition by name - Full Control
        var rdFullControl = subCtx.get_web().get_roleDefinitions().getByName('Full Control');

        //get roleDefinitionCollection object
        var collContribute = SP.RoleDefinitionBindingCollection.newObject(subCtx);
        var collFullControl = SP.RoleDefinitionBindingCollection.newObject(subCtx);

        //add role definition - Contribute
        collContribute.add(rdContribute);
        collFullControl.add(rdFullControl);

        var assignments = subCtx.get_web().get_roleAssignments();

        // assign the group to the new RoleDefinitionBindingCollection.
        var roleAssignmentContribute = assignments.add(subWebGroup, collContribute);
        var roleAssignmentFullControl = assignments.add(subWebOwnerGroup, collFullControl);

        subCtx.load(subWebGroup);
        subCtx.load(subWebOwnerGroup);

        subCtx.executeQueryAsync(Function.createDelegate(this, function(sender, args){
            console.log('inside success createGroup');        
            resolve(subWebGroup);

        }), Function.createDelegate(this, function(sender, args){
            console.log('Error to create group');
            reject('Error ' +args);
        }));
    });
}


function updateGroup(){
    //get siteName project
    let siteName = $('#txtRXPProjectName').val();
    
    //define group names
    let groupSiteMembers = `${siteName} Members`;
    let groupSiteOwners = `${siteName} Owners`;

    return new Promise((resolve, reject) => {

        //get all groups
        let siteGroups = subCtx.get_web().get_siteGroups();
        //get group by name
        let ownerGroup = siteGroups.getByName(groupSiteOwners);
        let memberGroup = siteGroups.getByName(groupSiteMembers);
        
        //set Owner group to be the owner of the site
        let oGroup = subCtx.get_web().get_siteGroups().getByName(groupSiteOwners);
        oGroup.set_owner(ownerGroup);
        oGroup.update();

        
        var mGroup = subCtx.get_web().get_siteGroups().getByName(groupSiteMembers);
        mGroup.set_owner(ownerGroup);
        mGroup.update();

        subCtx.load(oGroup);
        subCtx.load(mGroup);

        subCtx.executeQueryAsync(Function.createDelegate(this, function(sender, args){
                console.log('inside success getGroup');
                console.log(oGroup);        
                resolve(oGroup);

            }), Function.createDelegate(this, function(sender, args){
                console.log('Error to get group');
                reject('Error ' +args);
        }));
    });
}

function updatePermissionGroup() {
    //get siteName project
    let siteName = $('#txtRXPProjectName').val();
    //define group name
    let groupSiteOwners = `${siteName} Owners`;

    return new Promise((resolve, reject) => {

        //get all groups
        let siteGroups = subCtx.get_web().get_siteGroups();
        //get group by name
        let ownerGroup = siteGroups.getByName(groupSiteOwners);
        let permissionObj = new SP.BasePermissions();
        permissionObj.set(SP.PermissionKind.manageWeb);


    });
}

//defineContext function. Instantiate Context object.
function defineContext(strParam) {

        var ctx = new SP.ClientContext(strParam);
        var result = ctx.executeQueryAsync(Function.createDelegate(this, function(sender, args) {
                console.log('success define client object1');
            }),
            Function.createDelegate(this, function(sender, args) {
                alert('failed to define object');
            })
        );

        return ctx;
}

function defineSubContext(strParam) {
    return new Promise((resolve, reject) => {
        subCtx = new SP.ClientContext(strParam);

        subCtx.executeQueryAsync(Function.createDelegate(this, function(sender, args) {
                console.log('success define subClient object');
                resolve(subCtx);
            }),
            Function.createDelegate(this, function(sender, args) {
                alert('failed to define subClient object');
                reject(args);
            })
        );
    })
}