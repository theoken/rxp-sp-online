### Author: Kenny.soetjipto
#### SharePoint Developer
#### RXP Services
* kenny.soetjipto@rxpservices.com
 
### Description ###
This repository contains scripts to automate SharePoint online administration

### Quick summary ###
It is used to automate SP online administration and tasks are made into functions based at each file. 
Therefore, each function can be invoked using for or foreach loop

### RXPCreateSite ###
* It contains javascript to create project subsite.

### CreateProject.html ###
* It contains plain HTML form which need to be inserted.
* Insert new webpart > edit source.
* Copy and paste content from the CreateProject.html
* Ensure there is not any auto generate <div> tag inside the code beside <div> for error field

### How do I get set up? ###

* Summary of set up
- It needs to have Powershell online
- You must be assigned as "global administrator" role on the SharePoint Online site.
- Add ISAPI folder which contain DLLs needed

### Configuration - Powershell ###
- Put ISAPI folders in the C:\program files\common files\Microsoft Shared\Web Server Extension\15\
- If DLL files are already there, then you do not need to put that ISAPI Folder in there. 
- I provide it in case the user does not have it
- Change the $gUrl with your own SP Online URL site collection
- Change the $adminUrl with your SP Online admin page url
- Change $username with your own username
- Create PassFile.ps1 and put this code: $password = "your_password"
- Change $files values with path of the ISAPI folder in InitialConnectionScript.ps1

### Configuration - Gulp ###
- Create **settings.js** file and put code below:

```
#!javascript
module.exports = {
	username: 'your-username',
	password: 'your-own-password'
}

```

- Update task 'spUpload' in **gulpfile.js** for property:

```
#!javascript
siteUrl: 'sharepoint-online-url',
folder: 'SiteAssets/Javascripts',

```