﻿if ((Get-Module Microsoft.Online.SharePoint.PowerShell).Count -eq 0) 
{
    Import-Module Microsoft.Online.SharePoint.PowerShell -DisableNameChecking
}

Set-ExecutionPolicy bypass -Force 

#array to hold DLL files path
[string []] $files = @("C:\Program Files\Common Files\microsoft shared\Web Server Extensions\16\ISAPI\Microsoft.SharePoint.Client.dll",
            "C:\Program Files\Common Files\microsoft shared\Web Server Extensions\16\ISAPI\Microsoft.SharePoint.Client.Runtime.dll",
            "C:\Program Files\Common Files\microsoft shared\Web Server Extensions\16\ISAPI\Microsoft.SharePoint.Client.Taxonomy.dll"
            );

#iterate the DLL and check if the files are exist
foreach($file in $files)
{
    Get-ChildItem $file
    
    if(Test-Path $file)
    {
        Add-Type -Path $file
    }
    else
    {
        write-host "file is not exist. Download the DLL files and put into the C:\Program Files\Common Files\microsoft shared\Web Server Extensions\15\ISAPI folder"
    }   
}

#--------------------------------------------------------------------------------------------------------------------------------------
#OPENCONNECTION
#This function is used when SP Admin need to run -SPO cmdlet such as create site Collections
#--------------------------------------------------------------------------------------------------------------------------------------
function openConnection()
{    
    $adminUrl = "https://nsitechnology-admin.sharepoint.com" #CHANGE this Url        
    Connect-SPOService -Url $adminUrl -Credential $username

    write-host "Connection has opened" -BackgroundColor Green -ForegroundColor Black
}

# Insert the credentials and the name of the admin site
$Username="kenny.soetjipto@rxpservices.com"
$AdminPassword=Read-Host -Prompt "Password" -AsSecureString
$AdminUrl="https://nsitechnology.sharepoint.com"

$ctx=New-Object Microsoft.SharePoint.Client.ClientContext($AdminUrl)
$ctx.Credentials = New-Object Microsoft.SharePoint.Client.SharePointOnlineCredentials($Username, $AdminPassword)
$ctx.RequestTimeout = 7200000; 
$ctx.ExecuteQuery()

write-host "Initialize global variables" -BackgroundColor Cyan -ForegroundColor Black
$gWeb = $ctx.Web;
$ctx.Load($gWeb)
$ctx.ExecuteQuery()
write-host $gWeb.Title " has been initialized"
$gList = $ctx.Web.Lists;
$ctx.ExecuteQuery()
Write-Host $gList.ToString() " has been initialized"
$gField = $ctx.Web.Fields;
$ctx.Load($gField)
$ctx.ExecuteQuery()
write-host $gField

#INVOKE OpenConnection function to SP online in order to execute built-in SPO- cmdlet
openConnection

