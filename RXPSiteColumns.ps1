﻿#It contains initialize all global variables
. "C:\projects\RxPInternalProductionScript\RXPSiteColumnsObjects.ps1"
#it is needed to add SC into CT
. "C:\projects\RxPInternalProductionScript\RXPContentType.ps1"

#----------------------------------------------------------------------------- 
# Author   : Kenny Soetjipto
# Job Title: SharePoint Developer
# Company  : RXPServices
#----------------------------------------------------------------------------- 

#----------------------------------------------------------------------------- 
# Description: 
# This file contains any Site Columns operational
# It does not contain all columns. It contains common site columns
# If needed, copy and paste any functions below and change the FieldType value
#----------------------------------------------------------------------------- 

#--------------------------------------------------------------------------------------------------------------------------------------
# Functions available:
# TEXTSITECOLUMN, METADATASITECOLUMN, NUMBERSITECOLUMN, CHOICESITECOLUMN, CURRENCYSITECOLUMN
# DATETIMESITECOLUMN, USERSITECOLUMN, INSERTSITECOLUMN, SHOWALLCOLUMNS
#--------------------------------------------------------------------------------------------------------------------------------------

#--------------------------------------------------------------------------------------------------------------------------------------
# TEXTSITECOLUMN
# function to add [Text] SiteColumn
# it gives DisplayName and Name with same value(optional)
# It puts in Custom Column group(optional)
# It creates SINGLE LINE OF TEXT for FieldType
# Add Description property in XML if necessary
#--------------------------------------------------------------------------------------------------------------------------------------
function textSiteColumn([string]$columnName, [string] $required, [string] $desc)
{

    #$columnName="KennyCol1" #I used this for test
    #$Name="KennyCol1" #I used this for test
    
    $Group='RXP Columns' #optional. Can be changed
    #$required = "False"
     
    $xmlField = "<Field Type='Text' DisplayName='$columnName' Description='$desc' Name='$columnName' Required='$required' MaxLength='255' Group='$Group'/>"
    
    #INVOKE insertSiteColumn function
    insertSiteColumn $columnName $xmlField   
}

#--------------------------------------------------------------------------------------------------------------------------------------
# MULTILINEPLAINTEXTCOLUMN
# function to add [Multi line Text] SiteColumn
# It puts in Custom Column group(optional)
# It creates MULTI LINE PLAINTEXT for FieldType
# Default number of Lines: 5
#--------------------------------------------------------------------------------------------------------------------------------------
function multiLinePlainTextColumn([string] $columnName, [string] $required, [string] $desc)
{
    $richtext = "FALSE";
    $Group='RXP Columns' #optional. Can be changed
    $numberoflines = 5;

    #$xmlField = "<Field Type='Note' DisplayName='$columnName' Name='$columnName' required='FALSE' 
      #              NumLines='$numberoflines' RichText='$richtext' Group='$Group' />"
    
    $xmlField = "<Field Type='Note' DisplayName='$columnName' Name='$columnName' Description='$desc' Required='$required' NumLines='$numberoflines' RichText='$richtext' Group='$Group'/>"
    
    #INVOKE insertSiteColumn function
    insertSiteColumn $columnName $xmlField
}

#--------------------------------------------------------------------------------------------------------------------------------------
# MULTILINERICHTEXTCOLUMN
# function to add [Multi line Text] SiteColumn
# It puts in Custom Column group(optional)
# It creates MULTI LINE RICHTEXT for FieldType
# Default number of Lines: 5
#--------------------------------------------------------------------------------------------------------------------------------------
function multiLineRichTextColumn([string] $columnName, [string] $required, [string] $desc)
{
    $richtext = "TRUE";
    $Group='RXP Columns' #optional. Can be changed
    $numberoflines = 5;
        
    $xmlField = "<Field Type='Note' DisplayName='$columnName' Name='$columnName' Description='$desc' Required='$required' NumLines='$numberoflines' RichText='$richtext' Group='$Group'/>"
    
    #INVOKE insertSiteColumn function
    insertSiteColumn $columnName $xmlField
}

#--------------------------------------------------------------------------------------------------------------------------------------
# MULTILINEENHANCERICHTEXTCOLUMN
# function to add [Multi line Text] SiteColumn
# It puts in Custom Column group(optional)
# It creates MULTI LINE RICHTEXT for FieldType
# Default number of Lines: 5
#--------------------------------------------------------------------------------------------------------------------------------------
function enhanceMultiLineTextColumn([string] $columnName, [string] $required, [string] $desc)
{
    $richtext = "TRUE";
    $Group='RXP Columns' #optional. Can be changed
    $numberoflines = 5;
   
    $xmlField = "<Field Type='Note' DisplayName='$columnName' Name='$columnName' Description='$desc' Required='$required' NumLines='$numberoflines' RichTextMode='FullHtml' RichText='$richtext' Group='$Group'/>"
    
    #INVOKE insertSiteColumn function
    insertSiteColumn $columnName $xmlField
}

#--------------------------------------------------------------------------------------------------------------------------------------
# METADATASITECOLUMN
# function to add [Text] SiteColumn
# it gives DisplayName and Name with same value(optional)
# It puts in Custom Column group(optional)
# $multiVal = TRUE / FALSE - Specify multiple value or not
#--------------------------------------------------------------------------------------------------------------------------------------
function metadataSiteColumn([string] $columnName, [string] $required, [string] $multiVal, [string] $desc)
{
    #$columnName = "KennyMetadata1"
    #$multival = "TRUE"
    $Group='RXP Columns'
    $xmlField = "<Field Type='TaxonomyFieldType' DisplayName='$columnName' Name='$columnName' Description='$desc' Mult= '$multival' Required='$required' Group='$Group'/>"
    Write-Host $xmlField

    #INVOKE insertSiteColumn function
    insertSiteColumn $columnName $xmlField
}

#--------------------------------------------------------------------------------------------------------------------------------------
# NUMBERSITECOLUMN
# function to add [Number] SiteColumn
# it gives DisplayName and Name with same value(optional)
# It puts in Custom Column group(optional)
#--------------------------------------------------------------------------------------------------------------------------------------
function numberSiteColumn([string] $columnName, [string] $required)
{
    $Group='RXP Columns'
    $xmlField = "<Field Type='Number' DisplayName='$columnName' Name='$columnName' Description='$desc' Required='$required' Group='$Group' />"

    #INVOKE insertSiteColumn function
    insertSiteColumn $columnName $xmlField
}
#--------------------------------------------------------------------------------------------------------------------------------------
# HYPERLINKCOLUMN
# function to create Hyperlink / URL
# it gives DisplayName and Name with same value(optional)
# It puts in Custom Column group(optional)
#--------------------------------------------------------------------------------------------------------------------------------------
function hyperlinkColumn([string] $columnName)
{
    $group = "RXP Columns"
    $xmlField = "<Field Type='URL' DisplayName='$columnName' Name='$columnName' Group='$group' />"

    insertSiteColumn $columnName $xmlField
}
#--------------------------------------------------------------------------------------------------------------------------------------
# CHOICESITECOLUMN
# function to add [Choice] SiteColumn
# it gives DisplayName and Name with same value(optional)
# It puts in Custom Column group(optional)
# to use this:
# provide columnName 
# provide values for choices with ; for each choice
# example: choiceSiteColumn choiceColumn "choice1;choice2;choice3"
# format options: RadioButtons, Dropdown (default)
#--------------------------------------------------------------------------------------------------------------------------------------
function choiceSiteColumn([string] $columnName, [string]$values, [string]$format, [string] $required, [string] $desc)
{
    $Group = "RXP Columns"
    $valueArray = $values.Split(";")
    
    foreach($val in $valueArray)
    {
        $options = $options+ "<CHOICE>$val</CHOICE>"
    }

    $xmlField = "<Field Type='Choice' DisplayName='$columnName' Name='$columnName' Format='$format' Description='$desc' Required='$required' Group='$Group'><CHOICES>$options</CHOICES> </Field>"

    Write-Host $xmlField

    #INVOKE insertSiteColumn function
    insertSiteColumn $columnName $xmlField
}
#--------------------------------------------------------------------------------------------------------------------------------------
# Yes/No Choice SiteColumn
# It uses CHOICESITECOLUMN() above
#--------------------------------------------------------------------------------------------------------------------------------------
function yesNoChoiceSiteColumn([string] $columnName, [string] $desc)
{
    $values = "Yes;No"
    $format = "RadioButtons"
    $required = "False"
    #$columnName = "KennyChoice1"
    

    #INVOKE insertSiteColumn function
    choiceSiteColumn $columnName $values $format $required $desc
}

#--------------------------------------------------------------------------------------------------------------------------------------
# CURRENCYSITECOLUMN
# function to add [Currency] SiteColumn
# it gives DisplayName and Name with same value(optional)
# It puts in Custom Column group(optional)
#--------------------------------------------------------------------------------------------------------------------------------------
function currencySiteColumn([string] $columnName, [string] $required, [string] $desc)
{
    $Group = "RXP Columns"
    $xmlField = "<Field Type='Currency' DisplayName='$columnName' Name='$columnName' Description='$desc' Required='$required' Group='$Group' />"
    
    Write-Host $xmlField

    #INVOKE insertSiteColumn function
    insertSiteColumn $columnName $xmlField
}

#--------------------------------------------------------------------------------------------------------------------------------------
# DATETIMESITECOLUMN
# function to add [DateTime] SiteColumn
# it gives DisplayName and Name with same value(optional)
# It puts in Custom Column group(optional)
#--------------------------------------------------------------------------------------------------------------------------------------
function dateTimeSiteColumn([string] $columnName, [string] $required, [string] $desc)
{

    $Group = "RXP Columns"
    $xmlField = "<Field Type='DateTime' DisplayName='$columnName' Name='$columnName' Description='$desc' Required='$required' Group='$Group' />"
    
    Write-Host $xmlField

    #INVOKE insertSiteColumn function
    insertSiteColumn $columnName $xmlField
}

#--------------------------------------------------------------------------------------------------------------------------------------
# DATESITECOLUMN
# function to add [Date] SiteColumn only
# it gives DisplayName and Name with same value(optional)
# It puts in Custom Column group(optional)
#--------------------------------------------------------------------------------------------------------------------------------------
function dateSiteColumn([string] $columnName, [string] $required, [string] $desc)
{

    $Group = "RXP Columns"
    $xmlField = "<Field Type='DateTime' DisplayName='$columnName' Format= 'DateOnly' Name='$columnName' Description='$desc' Required='$required' Group='$Group' />"

    #INVOKE insertSiteColumn function
    insertSiteColumn $columnName $xmlField
}

#--------------------------------------------------------------------------------------------------------------------------------------
# USERSITECOLUMN
# function to add [UserSite] SiteColumn
# it gives DisplayName and Name with same value(optional)
# It puts in Custom Column group(optional)
# Selection Mode = text. 0 -> Only the name of the individual can be selected. 1 -> name and group can be selected
# $multiVal = TRUE / FALSE - Specify multiple value or not
#--------------------------------------------------------------------------------------------------------------------------------------
function userSiteColumn([string] $columnName, [string]$multi, [int] $selectionMode)
{
    $Group = "RXP Columns"
    $xmlField = "<Field Type='UserMulti' DisplayName='$columnName' Name='$columnName' StaticName='$fieldName' 
    UserSelectionScope='0' UserSelectionMode='$selectionMode' Sortable='FALSE' Required='FALSE' 
    Mult='$multi' Group='$Group'/>"

    #INVOKE insertSiteColumn function
    insertSiteColumn $columnName $xmlField
}

#--------------------------------------------------------------------------------------------------------------------------------------
# TESTSITECOLUMN
# function to test whether site column exist or not before insert into SP Online
# it is used in the insertSiteColumn
#--------------------------------------------------------------------------------------------------------------------------------------
function testSiteColumn([string] $columnName)
{	
    $columns = $gWeb.Fields
    $ctx.Load($columns)

    try
    {
        $ctx.ExecuteQuery()
        
    }
    catch [Net.WebException]
    {
        Write-Host $_.Exception.ToString()
    }

    $columnNames = $columns | select -ExpandProperty Title
    $exists = ($columnNames -contains $columnName)
    return $exists
}

#--------------------------------------------------------------------------------------------------------------------------------------
# INSERTSITECOLUMN
# function which insert site column into SP ONLINE
# this function is invoked in other type of site column functions
# AddToNoContentType - Enumeration whose values specify that a new field is not added to any content type. 
# Thus, can be added Manually
#--------------------------------------------------------------------------------------------------------------------------------------
function insertSiteColumn([string] $columnName, [string]$xmlField)
{
    if(!(testSiteColumn $columnName))
    {
        write-host "Create column object: $columnName" -BackgroundColor Green -ForegroundColor Black
        $columnOption = [Microsoft.SharePoint.Client.AddFieldOptions]::AddToNoContentType
        $siteColumn = $gField.AddFieldAsXml($xmlField, $true, $columnOption);

        try
        {
            $ctx.ExecuteQuery()
            Write-Host "Content Type: " $columnName " has been added to " $AdminUrl -ForegroundColor black -BackgroundColor Green
        }
        catch [Net.WebException]
        {
            Write-Host $_.Exception.ToString()
        }
    }
    else
    {
        write-host "Site column: $columnName already exist" -ForegroundColor black -BackgroundColor Yellow
    }
}

#--------------------------------------------------------------------------------------------------------------------------------------
# SHOWALLCOLUMNSINLIST
# get all columns from the list specified in parameter
#--------------------------------------------------------------------------------------------------------------------------------------
function showAllColumnsInList($listName)
{
    #$listName = "AnnouncementTest"
    $list = $gWeb.Lists.getByTitle($listName)
    $columns = $list.Fields
    $ctx.Load($columns)
    $ctx.ExecuteQuery()    

    foreach($item in $columns)
    {
	    write-host $item.Title
    }
}

#--------------------------------------------------------------------------------------------------------------------------------------
# SHOWALLCOLUMNS
# get all columns from the current URL which are specified in Global URL
# It is used to show URL, Columns in DEV
#--------------------------------------------------------------------------------------------------------------------------------------
function showAllColumns()
{    
    $gField | select Title, TypeDisplayName

    write-host "`nAll columns above are retrieved from: " -BackgroundColor Cyan -ForegroundColor Black
    write-host $AdminUrl -BackgroundColor Green -ForegroundColor Black 
}

#--------------------------------------------------------------------------------------------------------------------------------------
# RemoveSC
# Remove Site column using Title
#--------------------------------------------------------------------------------------------------------------------------------------

function removeSC([string] $fieldName)
{
    $sc = $gField.GetByTitle($fieldName);
    $sc.deleteObject();

     try
    {
        $ctx.ExecuteQuery()
        Write-Host "Site Column: " $fieldName " has been deleted to " $AdminUrl -ForegroundColor black -BackgroundColor Green
    }
    catch [Net.WebException]
    {
        Write-Host $_.Exception.ToString()
    }
}

#--------------------------------------------------------------------------------------------------------------------------------------
# RemoveBulkSC
# Remove bulk of Site columns using Title
#--------------------------------------------------------------------------------------------------------------------------------------
function removeBulkSC([string[]] $scNames)
{
    for($i = 0; $i -ne $scNames.Count; $i++)
    {
        removeSC $scNames[$i]
    }
}


#--------------------------------------------------------------------------------------------------------------------------------------
# ADDSCTOCT
# Remove bulk of Site columns using Title
#--------------------------------------------------------------------------------------------------------------------------------------
function addSCToCT([string] $scName, [string] $ctToAdd)
{
    Write-Host "Content type destination: $ctToAdd"
    Write-Host "Source for Site Column: $scName"

    if(testSiteColumn($scName)) #Test if Site Column is exist
    {
        Write-Host "Site Column is exist" -BackgroundColor Cyan -ForegroundColor Black        
        
        if(testCT($ctToAdd)) #Test if Content Type is exist
        {
            Write-Host "Content type is exist" -BackgroundColor Cyan -ForegroundColor Black
            $field = $gField.GetByTitle($scName)
            $ctx.Load($field)
            #Get All CT id and assign into variable
            $ctID = getAllCT
            #Get CT name
            $ctName = $ctID | Where-Object {$_.Name -eq $ctToAdd}
            $destCTIdStr = $ctName.Id.ToString();
            $destCT = $gWeb.ContentTypes.GetById($destCTIdStr)
            $ctx.Load($destCT)

            $fieldRef = New-Object Microsoft.SharePoint.Client.FieldLinkCreationInformation
            $fieldRef.Field = $field
            $destCT.FieldLinks.Add($fieldRef);
            $destCT.Update($true)

            try
            {
                Write-Host "Adding Site Column into Content type"
                $ctx.ExecuteQuery()
                Write-Host "SC: $scName has been added into content type $ctToAdd in address $AdminUrl" -ForegroundColor Cyan -BackgroundColor Black 

            }
            catch [Net.WebException]
            {
                Write-Host $_.Exception.ToString() -BackgroundColor Red -ForegroundColor Black
            }
        }
        else
        {
            Write-Host "Content Type is not exist" -BackgroundColor Red -ForegroundColor Black
        }
    }
    else
    {
        write-host "Site Column is not exist" -BackgroundColor Red -ForegroundColor Black
    }    
}

function addBulkSCtoCT([string[]] $scNames, [string] $ctToAdd)
{
    Write-Host "Add bulk of Site Columns" -BackgroundColor Cyan -ForegroundColor Black

    for($i = 0; $i -ne $scNames.Count; $i++)
    {
        addSCToCT $scNames[$i] $ctToAdd
    }
}

function exeCTX([Microsoft.SharePoint.Client.ClientContext] $ctxParam)
{
    try
    {
        $ctxParam.ExecuteQuery()
    }
    catch [Net.WebException]
    {
        Write-Host "Can't instantiate Client Object" -BackgroundColor Red -ForegroundColor Black
        Write-Host $_.Exception.ToString()
    }
}

function showAllColumnsInList()
{
    $listName = "Projects"
    $AdminUrl = "https://nsitechnology.sharepoint.com/sites/Engagement/ProjectPortal"
    $ctx=New-Object Microsoft.SharePoint.Client.ClientContext($AdminUrl)
    $ctx.Credentials = New-Object Microsoft.SharePoint.Client.SharePointOnlineCredentials($Username, $AdminPassword)
    $ctx.RequestTimeout = 7200000; #Set the timeout to be 2 hours at least
    $ctx.ExecuteQuery()

    $gWeb = $ctx.Web;
    $ctx.Load($gWeb)
    $ctx.ExecuteQuery()
    
     #$listName = "AnnouncementTest"
    $list = $gWeb.Lists.getByTitle($listName)
    $columns = $list.Fields
    $ctx.Load($columns)
    $ctx.ExecuteQuery()    

    $columns | select Title, InternalName

    foreach($item in $columns)
    {
	    write-host $item.Title
    }
}