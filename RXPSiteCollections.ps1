﻿#include Password file into this script. This file path is outside the project folder
. "C:\projects\RxPInternalProductionScript\RXPSiteCollectionObjects.ps1"

#-------------------------------------------------------------------------------------------------------------------------------------- 
# Description: 
# This file contains functions to create Site collections
# Site Collections are:
# Team, Project
# It gives default allocation memory of 500MB but can be changed in the CREATESITE() function
# It gives prefix sites at the middle before the site name. Ex: http://.../sites/...
# Template reference link:
# http://www.funwithsharepoint.com/sharepoint-2013-site-templates-codes-for-powershell/
#-------------------------------------------------------------------------------------------------------------------------------------- 

#--------------------------------------------------------------------------------------------------------------------------------------
#TEAM
#function to create a site with template of TEAM
#Provide sitename, title and owner values as parameters
#--------------------------------------------------------------------------------------------------------------------------------------
function teamSite([string]$siteName,[string]$title, [string]$owner)
{
    $templateType = "STS#0"

    #invoke CREATESITE() function 
    createSite $siteName $title $templateType $owner
}

#--------------------------------------------------------------------------------------------------------------------------------------
# PROJECT
#function to create a site with template of PROJECT
#Provide sitename, title and owner values as parameters
#--------------------------------------------------------------------------------------------------------------------------------------
function projectSite([string]$siteName,[string]$title, [string]$owner)
{    
    $templateType = "PROJECTSITE#0"; 
    
    createSite $siteName $title $templateType $owner     
}

#--------------------------------------------------------------------------------------------------------------------------------------
# CREATESITE
# Parameters: string $siteName, string $template, string $owner 
# A function which actually create the site in SP Online
# Default memory: 500 Mb
# This function is not called directly but it is called by other sites function above
#--------------------------------------------------------------------------------------------------------------------------------------
function createSite([string]$siteName, [string]$title, [string]$template, [string]$owner)
{
    #Ensure that connection is open.It will ask user's credentials
    #openConnection  
    
    $stQuota = 20000  #20GB 
    $sitesList = Get-SPOSite
    $rootUrl = $sitesList[0];
    $rootUrlValue = $rootUrl | select -ExpandProperty url

    $url = $rootUrlValue+ "sites/" +$siteName    

    #invoke TESTSITE() function. If the site is not exist, then create new SC
    if(!(testSite($url)))
    {             
        New-SPOSite -Url $url -title $title -Owner $owner -Template $template -StorageQuota $stQuota
        write-host "The site $siteName has created" -ForegroundColor Black -BackgroundColor Cyan
    }
    else #if the site is exist
    {             
        write-host "Site is already exist in SP online" -ForegroundColor Red -BackgroundColor Yellow
    }
}

#--------------------------------------------------------------------------------------------------------------------------------------
# CREATESUBSITE
# Parameters: string $title, string $parentsite, string $sitename 
# A function which create the SubSite in SP Online
# Default memory: 20 GB
# This function is needed because CREATESITE can't be used to create subsite
# Default: Uses Team site template, and same permission as parent site
#--------------------------------------------------------------------------------------------------------------------------------------
function createSubSite([string] $title, [string]$parentSite, [string]$sitename, [string]$template = "STS#0", [bool]$permissionParent = $true)
{
    #Quota of 20GB
    $stQuota = 20000    

    if((testSite $parentSite))
    {
        Write-Host "Parent site is exist" -BackgroundColor Cyan -ForegroundColor Black

        #define Context object with new URL
        $ctx=New-Object Microsoft.SharePoint.Client.ClientContext($parentSite)

        $ctx.Credentials = New-Object Microsoft.SharePoint.Client.SharePointOnlineCredentials($Username, $AdminPassword)
        $ctx.RequestTimeout = 7200000; 
        $ctx.ExecuteQuery()

        #get all sites
        $siteLists = Get-SPOSite | select Url
        #retrieve url for the parentSite URL
        $parentUrl = $siteLists | Where-Object {$_.Url -eq $parentSite} | select -ExpandProperty Url        

        #Define properties of the subsite
        Write-Host "Define properties for the new subsite" -BackgroundColor Green -ForegroundColor Black
        $newSubSite = New-Object Microsoft.SharePoint.Client.WebCreationInformation
        $newSubSite.Title = $title
        $newSubSite.Url = $sitename
        $newSubSite.WebTemplate = $template
        $newSubSite.UseSamePermissionsAsParentSite = $permissionParent

        $newSite = $ctx.Web.Webs.Add($newSubSite)

        try
        {
            $ctx.ExecuteQuery();
            Write-Host "The subsite has created" -BackgroundColor Cyan -ForegroundColor Black
        }
        catch
        {
            Write-Host "Fail to create" -BackgroundColor Red -ForegroundColor Black
        }     
        
    }
    else
    {
        Write-Host "The parent Site is not exist" -ForegroundColor Red -BackgroundColor Black
    }    

}

#--------------------------------------------------------------------------------------------------------------------------------------
# TESTSITE
#Is used to test whether the site is exist in SP Online
#Is invoked in CREATESITE() function
#if exists return true else return false
#--------------------------------------------------------------------------------------------------------------------------------------
function testSite([string]$siteName)
{        
    $sites = Get-SPOSite
    $siteNames = $sites | select -ExpandProperty Url
    $exists = ($siteNames -contains $siteName) 
    return $exists
}

function getRootSiteCollection()
{
    #openConnection
    $sitesList = Get-SPOSite
    $rootUrl = $sitesList[0];
    $rootUrlValue = $rootUrl | select -ExpandProperty url
    return $rootUrlValue;
}

