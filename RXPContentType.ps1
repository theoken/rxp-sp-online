﻿#include RXPContentTypeObjects file.
. "C:\projects\RxPInternalProductionScript\RXPContentTypeObjects.ps1"

#--------------------------------------------------------------------------------------------------------------------------------------
#TESTCT
#Is used to test whether content type is exist or not
#PARAM: string 
# RESULT: False -> not exist, true -> exist
#--------------------------------------------------------------------------------------------------------------------------------------
function testCT([string]$ctName)
{
    #$ctName = "Document"
    $cts = $gWeb.ContentTypes
    $ctx.Load($cts)
    $ctx.ExecuteQuery()
    $result = $cts | select -ExpandProperty Name
    $exists = ($result -contains $ctName)
    return $exists
}

#--------------------------------------------------------------------------------------------------------------------------------------
#SHOWALLCT
#Is used to show all content types available.
#It shows Name and ID of each content type
#--------------------------------------------------------------------------------------------------------------------------------------
function showAllCT()
{
    $ctID = $gWeb.ContentTypes
    $ctx.Load($ctID)
    $ctx.ExecuteQuery()
    $ctID | select Name, Id    
}
#--------------------------------------------------------------------------------------------------------------------------------------
#GETAllCT
#is used to get all content types Name and ID
#$ctypeID has name and id and GLOBAL
#$gCTypes has contentTypes object and GLOBAL
#RESULT: return content type of Name and Id
#--------------------------------------------------------------------------------------------------------------------------------------
function getAllCT()
{
    $Global:gCTypes = $gWeb.ContentTypes
    $ctx.Load($gCTypes)
    try
    {
        $ctx.ExecuteQuery()
    }
    catch [Net.WebException]
    {
        Write-Host $_.Exception.ToString()
    }
    $ctx.ExecuteQuery()
    $Global:ctypeID = $gCTypes | select Name, Id
    return $ctypeID  
}

#--------------------------------------------------------------------------------------------------------------------------------------
#GETCT
#is used to get content type specifically define in $param variable
#$ctypeID has name and id and GLOBAL
#$gCTypes has contentTypes object and GLOBAL
#PARAM: string
#RESULT: return content type match with parameter specified
#--------------------------------------------------------------------------------------------------------------------------------------
function getCT([string] $ctName)
{
    $ctID = getAllCT    
    $ctResult = $ctID | Where-Object {$_.Name -eq $ctName}
    return $ctResult 
}

#--------------------------------------------------------------------------------------------------------------------------------------
#removeCT
#is used to remove content type
#PARAM: string
#--------------------------------------------------------------------------------------------------------------------------------------
function removeCT([string] $Name)
{
    #$Name = "Email"
    $ctID = getAllCT    
    $ctName = $ctID | Where-Object {$_.Name -eq $Name} 
    $ctIDString = $ctName.Id.ToString()
    $ctNameString = $ctName.Name.ToString()

    if(testCT($ctNameString))
    {
        $ctx.Web.ContentTypes.GetById($ctIDString).DeleteObject();

        try
        {
            $ctx.ExecuteQuery()               
            Write-Host "Content type is deleted" -BackgroundColor Green -ForegroundColor Black
        }
        catch [Net.WebException]
        {
            Write-Host $_.Exception.ToString()
        }
        
    }
    else
    {
        write-host "Content Type Not exist" -BackgroundColor red -ForegroundColor Black
    }    
}

#--------------------------------------------------------------------------------------------------------------------------------------
#CREATECT
#is used to create Content Type and is not invoked directly
#is called by other CTs functions.
#PARAM: string, string, string
#--------------------------------------------------------------------------------------------------------------------------------------
function createCT([string] $name, [string] $parentCTName, [string] $description)
{
	#$parentCTName = "Document";
    #$name = "Test2"
    #$description = "Test2 Description";    
    $group = "RXP Content Types";
       
    
	if(testCT($parentCTName))
	{
		write-host "Parent Content type: $parentCTName is exist" -BackgroundColor Green -ForegroundColor Black
        
        if(!(testCT($name)))
        {
            Write-Host "CT $name is not exist" -BackgroundColor Green -ForegroundColor Black
            Write-Host "Create CT: $name" -BackgroundColor Green -ForegroundColor Black
            
            #get parent CT ID
            write-host "Get Parent CT ID" -BackgroundColor Cyan -ForegroundColor Black
            $ctID = getAllCT
            $ctsName = $ctID | Where-Object {$_.Name -eq $parentCTName}            
            $parentCTID = $ctsName.Id.ToString();
            Write-Host "Parent CT Name: " $parentCTName
            Write-Host "Parent CT Id: " $parentCTID

            Write-Host "Define new CT properties" -BackgroundColor Cyan -ForegroundColor Black
            
            Write-Host "New CT: " $name -BackgroundColor Cyan -ForegroundColor Black
            Write-Host "Parent CT: " $parentCTName -BackgroundColor Cyan -ForegroundColor Black
            Write-Host "New CT description: " $description -BackgroundColor Cyan -ForegroundColor Black 
            Write-Host $ctx.Web.ContentTypes.GetById($parentCTID)
                                   
            $lci =New-Object Microsoft.SharePoint.Client.ContentTypeCreationInformation
            $lci.Description=$description
            $lci.Name=$name            
            $lci.ParentContentType = $ctx.Web.ContentTypes.GetById($parentCTID)
            $lci.Group = $group
                        
            $contentType = $ctx.Web.ContentTypes.Add($lci)
            $contentType.Update($true)
            
            $ctx.Load($contentType)
            
            try
            {        
                $ctx.ExecuteQuery()
                Write-Host "Content Type: " $Name " has been added to " $AdminUrl
            }
                catch [Net.WebException]
            {
                Write-Host $_.Exception.ToString()
            }     
        }
        else
        {
            write-host "Content type: $name is exist" -BackgroundColor Yellow -ForegroundColor Black
        }
	}
	else
	{
		write-host "Parent Content type is not exist"
	}
}
 
#--------------------------------------------------------------------------------------------------------------------------------------
#CREATERXPCTS
#Is used to create bulk of Content types
#invokes CREATECT function above which require parameters array
#PARAM: string, string, string 
#--------------------------------------------------------------------------------------------------------------------------------------
function createRXPCTs([string[]]$firstCTParam, [string] $parentCTParam, [string[]]$firstCTDescParam)
{
    write-host "Parent is: "$parentCTParam
    

    if(testCT($parentCTParam))
    {
        Write-Host "Creating Content Types" -BackgroundColor Green -ForegroundColor Black
        for($i = 0; $i -ne $firstCTParam.Count; $i++)
        {           
           Write-Host "New CT is: $firstCTParam[$i]"
           createCT $firstCTParam[$i] $parentCTParam $firstCTDescParam[$i] 
        }        
    }
    else
    {
        write-host "Parent Content Type Not exist" -BackgroundColor red -ForegroundColor Black
    }
}



