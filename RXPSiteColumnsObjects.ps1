﻿if ((Get-Module Microsoft.Online.SharePoint.PowerShell).Count -eq 0) 
{
    Import-Module Microsoft.Online.SharePoint.PowerShell -DisableNameChecking
}

Set-ExecutionPolicy bypass -Force 

#array to hold DLL files path
[string []] $files = @("C:\Program Files\Common Files\microsoft shared\Web Server Extensions\16\ISAPI\Microsoft.SharePoint.Client.dll",
            "C:\Program Files\Common Files\microsoft shared\Web Server Extensions\16\ISAPI\Microsoft.SharePoint.Client.Runtime.dll",
            "C:\Program Files\Common Files\microsoft shared\Web Server Extensions\16\ISAPI\Microsoft.SharePoint.Client.Taxonomy.dll"
            );

#iterate the DLL and check if the files are exist
foreach($file in $files)
{
    Get-ChildItem $file
    
    if(Test-Path $file)
    {
        Add-Type -Path $file
    }
    else
    {
        write-host "file is not exist. Download the DLL files and put into the C:\Program Files\Common Files\microsoft shared\Web Server Extensions\15\ISAPI folder"
    }   
}

#--------------------------------------------------------------------------------------------------------------------------------------
#OPENCONNECTION
#This function is used when SP Admin need to run -SPO cmdlet such as create site Collections
#--------------------------------------------------------------------------------------------------------------------------------------
function openConnection()
{    
    $adminUrl = "https://nsitechnology-admin.sharepoint.com" #CHANGE this Url        
    Connect-SPOService -Url $adminUrl -Credential $username

    write-host "Connection has opened" -BackgroundColor Green -ForegroundColor Black
}

#IMPORTANT to change user's credentials
# Insert the credentials and the name of the admin site.
$Username="kenny.soetjipto@rxpservices.com"
$AdminPassword=Read-Host -Prompt "Password" -AsSecureString

#IMPORTANT to change this url to appropriate URL
$AdminUrl="https://nsitechnology.sharepoint.com/sites/contentTypeHub"

#Create Client Context object
$ctx=New-Object Microsoft.SharePoint.Client.ClientContext($AdminUrl)
$ctx.Credentials = New-Object Microsoft.SharePoint.Client.SharePointOnlineCredentials($Username, $AdminPassword)
$ctx.RequestTimeout = 7200000; #Set the timeout to be 2 hours at least
$ctx.ExecuteQuery()

#Initialize Global variables
write-host "Initialize global variables" -BackgroundColor Cyan -ForegroundColor Black
$gWeb = $ctx.Web;
$ctx.Load($gWeb)
$ctx.ExecuteQuery()
write-host $gWeb.Title " has been initialized"
$gList = $ctx.Web.Lists;
$ctx.ExecuteQuery()
Write-Host $gList.ToString() " has been initialized"
$gField = $ctx.Web.Fields;

$ctx.Load($gField)
$ctx.ExecuteQuery()
write-host "Global Web.Field variable has been initialized"

#Setting: Optional = false, required = true

#YesNo variable
$yesNoVar = "Attachment";
$yesNoVarDesc = "Indicate whether an email content type has one or more attachments.";

#Multi Line variable
$multiLineVar = @("BCC", "BCC-Address", "CC", "CC-Address", "Contract With", "To", "To-Address", "Deadline Reason",
                  "Key Messages", "Notes", "Objectives", "Success Description", "Target Audience", "Reference Material");
$multiLineVarDesc = @("Capture the display names of recipients in the BCC email field.",
                      "Capture the email addresses of recipients in the BCC email field.",
                      "Capture the display names of any recipients in the CC email field.",
                      "Capture the email addresses of any recipients in the CC email field.",
                      "Specify companies / clients who are parties to a contract.",
                      "The display names of recipients in the `"To`" email field.",
                      "The email addresses of the recipients in the `"To`" email field.",
                      "Reason of deadline of request",
                      "Message within consultant request",
                      "Notes within consultant request",
                      "Objective of consultant request",
                      "Description of successful consultant request",
                      "Audience who receive the request",
                      "External resources as reference"
                      )
$multiLineVarSetting = @("FALSE", "FALSE", "FALSE", "FALSE", "TRUE", "TRUE", "TRUE", "FALSE", 
                         "FALSE", "FALSE", "FALSE", "FALSE", "FALSE", "FALSE")

#Single line variable.
$singleLineVar = @("Categories", "From", "From-Address", "Location", 
                    "Purchase Order Number", "Subject", "Publisher", "Budget available", "Client", "Employee ID", "Role Title",
                    "Contract With", "Start Date", "End Date", "Role title");

#Define array to hold all "single line description" site columns
$singleLineVarDesc = @("Capture any Categories associate with Email.",                        
                       "The display name of the sender of the email.",
                       "The email address of the sender.",
                       "The location of the events to be held at.",
                       "Unique ID number associate with purchase order.",
                       "Subject of the email.",
                       "Publisher of external reference document",
                       "Define availabity of the budget",
                       "Define name of the clients",
                       "The unique identifier for a specific RXP employee provided by the HR system",
                       "The position title of an RXP employee / contractor",
                       "Define which client the contract associate with",
                       "Start date of meeting document set",
                       "End date of meeting document set",
                       "Role title of the personnel") 

#Array to define required or optional
$singleLineVarSetting = @("FALSE", "TRUE", "TRUE", "TRUE", "TRUE", "TRUE", "TRUE", "FALSE", "FALSE", "TRUE", "FALSE", "TRUE", "FALSE");

#Date Only variable
$dateVar = @("Commencement Date", "Period Start", "Period End", "Termination Date", "Deadline")
$dateVarDesc = @("Commencement date of a contract.", 
                 "Start date of status report.",
                 "End date of status report.",
                 "The specific termination date of the contract.",
                 "Date of deadline")
$dateVarSetting = @("False", "TRUE", "TRUE", "FALSE", "FALSE")

#DateTime variable
$dateTimeVar = @("End Date", "Received", "Sent", "Start Date")
$dateTimeVarDesc = @("The date and time an event or item is scheduled to finish.",
                     "Date and time the email was received.",
                     "The date and time the email was sent.",
                     "Start date of particular event.")
$dateTimeVarSetting = @("TRUE", "TRUE", "TRUE", "TRUE")

#MMS variable. Financial Period need additional setting
$mmsVar = @("Financial Period", "Contract Status", "Reference Type", "Capability", 
            "Region", "Practice", "Practices Involved", "Product or Service", 
            "Discipline", "Employment Status");

$mmsVarDesc = @("Capture financial periods relevant to the item.",
                "Status of a contract.",
                "Type of reference documents",
                "Define capability relate to consultants",
                "Region of RXP consultants", 
                "RXP practice",
                "Specify practices involved in the consultant request",
                "RXP Product or Service",
                "Category of Reference document",
                "Status of the employment")

#Setting for required or optional
$mmsVarReqSetting = @("TRUE", "TRUE", "TRUE", "FALSE", "FALSE", "FALSE", "FALSE", "FALSE", "FALSE", "FALSE")
#Setting to allow multiple values
$mmsVarMultSetting = @("TRUE", "FALSE", "FALSE", "TRUE", "FALSE", "TRUE", "TRUE", "TRUE", "TRUE", "FALSE")

#Marketing brief request List selection
$materialReqName = "Material required"
$materialReqVal = @("Microsite", "Telemarketing Campaign", "Search Engine Marketing", "Email event invitation / Email newsletter", 
                    "Article / Blog Post", "Powerful Page Update", "Website Page Update", "A4 Flyer", "Event / Tradeshow Resources")
$format = "Dropdown"
$materalRegSetting = "FALSE"
$materialReqDesc = "Materials for Marketing Brief Request"

#Request Status List selection
$RequestStatusName = "Request Status"
$RequestStatusVal = @("New", "In Progress", "Rejected", "Completed")
$RequestStatusFormat = "Dropdown"
$RequestStatusSetting = "FALSE"
$RequestStatusDesc = "Request status for Marketing brief request"

$peoplePickerName = @("Consultant", "Assigned To", "Key Contact")
$peoplePickerMult = @("FALSE", "FALSE", "FALSE")
$peoplePickerSelectMode = @("0", "0", "0")


#Currency variable
$currencyVar = "Value"
$currencyVarDesc = "Dollar value of the item."
$currencyVarSetting = "FALSE" 

$urlVarName = "Source"

#----------------------------------------------------------------------------------------------------------------------------
# Additional Site Columns
#----------------------------------------------------------------------------------------------------------------------------
#Policy
$corpColName = "Policy Area"
$corpReq = "FALSE"
$corpMultVal = "TRUE"
$corpColDesc = "Category of policy based on area"

#Strategy
$corpStrategyColName = "Period"
$corpStrategyReq = "FALSE"
$corpStrategyMultVal = "TRUE"
$corpStrategyColDesc = "Category based on Financial period"

#Annual Report
$corpAnnualColName = "Period"
$corpAnnualReq = "FALSE"
$corpAnnualMultVal = "TRUE"
$corpAnnualColDesc = "Category based on Financial Period"
#----------------------------------------------------------------------------------------------------------------------------
# Content Type objects and site columns object area
#----------------------------------------------------------------------------------------------------------------------------
$ctPO = "Purchase Order"
$POscs = "Purchase Order Number"

$ctSR = "Status Report"
$SRscs = @("Period Start", "Period End")

$ctFD = "Financial Document"
$FDscs = @("Financial Period")

$ctEmail = "Email"
$Emailscs = @("Attachment", "BCC", "BCC-Address", "Categories", "CC", "CC-Address", "From", "From-Address", "Received", "Sent", "Subject", "To", "To-Address")

$ctContractDoc = "Contract Document"
$ContractDocscs = @("Contract With", "Commencement Date", "Contract Status", "Value")

$ctContract = "Contract"
$contractscs = @("Termination Date")

$ctMeetingDoc = "Meeting Document"
$meetingscs = @("Start Date", "End Date", "Location")

$ctReferenceDoc = "Reference Document"
$referenceDocscs = @("Reference Type", "Publisher", "Discipline", "Product or Service", "Source")

$ctMarketingDoc = "Marketing Document"
$ctMarketingDocscs = @("Product or Service", "Capability", "Practice")

$ctCaseStudy = "Case Study"
$ctCaseStudyscs = @("Client")

$ctConsultantProfile = "Consultant Profile"
$ctConsultantProfilescs = @("Consultant", "Region")

$ctRXPImage = "RXP Image"
$ctRXPImagescs = @("Product or Service", "Capability", "Practice")

$ctContractDocSet = "Contract Document Set"
$ctContractDocSetscs = @("Contract With", "Commencement Date", "Termination Date", "Contract Status", "Value")

$ctMeetingDocSet = "Meeting Document Set"
$ctMeetingDocSetscs = @("Start Date", "End Date", "Location")

$ctPersonnelRecord = "Personnel Record"
$ctPersonnelRecordscs = @("Employee ID", "Employment Status", "Practice", "Role Title")

$ctEmploymentContract = "Employment Contract"
$ctEmploymentContractscs = @("Commencement Date", "Termination Date")

$ctPersonnelDocument = "Personnel Document"
$ctPersonnelDocumentscs = @("Employee ID", "Employment Status", "Practice", "Role Title")

$ctMarketingBrief = "Marketing Brief Request"
$ctMarketingBriefscs = @("Request Status", "Assigned To", "Key Contact", "Practices Involved", "Deadline", "Deadline Reason", "Material required")


 