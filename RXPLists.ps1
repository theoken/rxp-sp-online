﻿. "C:\projects\RxPInternalProductionScript\RXPListsObjects.ps1"

#Reference link regard template type for List
# http://techtrainingnotes.blogspot.com.au/2008/01/sharepoint-registrationid-list-template.html

#--------------------------------------------------------------------------------------------------------------------------------------
# SHOWALLCOLUMNS
# get all columns from the current URL which are specified in Global URL
# It is used to show URL, Columns in DEV
#--------------------------------------------------------------------------------------------------------------------------------------
function showAllList([string] $siteUrl)
{
    $ctx = defineContext $siteUrl
    $gList = $ctx.Web.Lists
    $ctx.Load($gList)
    exeCtx $ctx
        
    $gList | select Title    
}

#--------------------------------------------------------------------------------------------------------------------------------------
# GETALLSPLIST
# get all SPLists as parameter specified
#--------------------------------------------------------------------------------------------------------------------------------------
function getAllList([string] $siteUrl)
{    
    $ctx = defineContext $siteUrl    
    $gList = $ctx.Web.Lists
    $listColl = $gList;
    $ctx.Load($listColl)
    $ctx.ExecuteQuery()

    $listColl = $gList;
    $ctx.Load($listColl)

    #Execute Query
    exeCtx $ctx
    
    $listColl | select Title
    return $listColl
}

#--------------------------------------------------------------------------------------------------------------------------------------
# ENABLECTALLLISTS
# Enable Content Types to All Lists based on SiteURL provided from the parameter
# PARAMETERS: siteUrl
#--------------------------------------------------------------------------------------------------------------------------------------
function enableCTAllLists([string] $siteUrl)
{
    #Define context object with new URL    
    $ctx = defineContext $siteUrl
    #Get the list object and assign into var    
    $gList = $ctx.Web.Lists
    $listColl = $gList;
    $ctx.Load($listColl)
        
    #Execute Query
    exeCtx $ctx
    
    #Get the title of the Lists and assign into variable        
    $x = $listColl | select -ExpandProperty Title
    
    #iterate Lists based on title and enable Content Type to true
    for($i = 0; $i -ne $x.Count; $i++)
    {
        Write-Host "Get List title" -BackgroundColor Green -ForegroundColor Black
        $lists = $ctx.Web.Lists.GetByTitle($x[$i])
        $ctx.Load($lists)
        #Execute Query
        exeCtx $ctx

        Write-Host "Enable content types" -BackgroundColor Green -ForegroundColor Black
        $lists.ContentTypesEnabled = $true;

        Write-Host "Activate change of the setting" -BackgroundColor Green -ForegroundColor Black
        $lists.Update()
        
        try
        {
            $ctx.ExecuteQuery()
        }
        catch [Net.WebException]
        {
            Write-Host "Failed to enable content type" -BackgroundColor Red -ForegroundColor Black 
        }   
    }
}

#--------------------------------------------------------------------------------------------------------------------------------------
# ADDCTTOLIST
# Adding content type to certain list based on Parameters of the function
# PARAMETERS: siteUrl, listTitle, ctName
#--------------------------------------------------------------------------------------------------------------------------------------
function addCTtoList([string]$siteUrl, [string] $listTitle, [string]$ctName)
{
    #$siteUrl = "https://nsitechnology.sharepoint.com/sites/ReferenceLibrary"
    #$listTitle = "Library"
    #$ctName = "Client Document"
    
    #Define content object with new URL site    
    $ctx = defineContext $siteUrl

    if(testList($listTitle))
    {
        if(testSpecificCT($ctName) -eq $null)
        {
            Write-Host "Define Context object using new URL: `n$siteUrl" -BackgroundColor Green -ForegroundColor Black
             
            #Get collection of lists object and assign into var
            Write-Host "Get the list object" -BackgroundColor Green -ForegroundColor Black    
            $gList = $ctx.Web.Lists
            $listColl = $gList;
            $ctx.Load($listColl)
        
            #Execute Query
            exeCtx $ctx        
            #Get specific List 
            $list = $listColl | Where-Object{$_.Title -eq $listTitle}    
            
            #Retrieve collection of CTs
            $cts = $list.ContentTypes        
            exeCtx $ctx

            #get collection content type object from Web object
            Write-Host "Get Content Type object: $ctName"
            $ctObj = $ctx.Web.ContentTypes
            $ctx.Load($ctObj)
    
            #Execute Query
            exeCtx $ctx

            #get specific content type
            $ctResult = $ctObj | Where-Object{$_.Name -eq $ctName}
    
            Write-Host "Adding Content type to List" -BackgroundColor Green -ForegroundColor Black
            $ctReturn = $cts.AddExistingContentType($ctResult)
    
            $ctx.Load($ctReturn)

            try
            {
                 $ctx.ExecuteQuery()
                 Write-Host "Content Type: $ctName has added into List: $listTitle" -BackgroundColor Cyan -ForegroundColor Black
            }
            catch
            {
                Write-Host "Can't add content type: $ctName into List: $listTitle" -BackgroundColor Red -ForegroundColor Black
            }      
            
        }
        else
        {
            Write-Host "CT: $ctName exist in the List: $listTitle" -ForegroundColor Red -BackgroundColor Black
        }       
    }
    else
    {
        Write-Host "The list is not exist" -BackgroundColor Red -ForegroundColor Black
    }           
}

#--------------------------------------------------------------------------------------------------------------------------------------
# GETSPLIST
# function to get specific List with return List type based on parameters provided
# PARAMETERS: listNameParam, siteUrl
#--------------------------------------------------------------------------------------------------------------------------------------
function getList([string]$listNameParam, [string] $siteUrl)
{
    #Create Client Context object
    #$siteUrl = "https://nsitechnology.sharepoint.com"   

    $ctx = defineContext $siteUrl
    
    $gList = $ctx.Web.Lists
    $ctx.Load($gList)
    $ctx.ExecuteQuery()
    $listColl = $gList;
    $ctx.Load($listColl)
    
    #Execute Query
    exeCtx $ctx
    
    $listResult = $listColl | Where-Object {$_.Title -eq $listNameParam}
    #$listResult              
    return $listResult;
}

#--------------------------------------------------------------------------------------------------------------------------------------
# CUSTOMLIST
# Function to create CUSTOM list using parameters provided by users
# PARAMETERS: listTitle, desc, siteUrl
#--------------------------------------------------------------------------------------------------------------------------------------
function customList([string]$listTitle,[string] $desc, [string] $siteUrl)
{
    $templateType = "GenericList"
    createList $siteUrl $listTitle $desc $templateType
}
#--------------------------------------------------------------------------------------------------------------------------------------
# ISSUETRACKLIST
# Function to create ISSUETRACK list
# PARAMETERS: listTitle, desc, siteUrl
#--------------------------------------------------------------------------------------------------------------------------------------
function issueTrackList([string]$listTitle,[string] $desc, [string] $siteUrl)
{
    $templateType = "Issue"
    createList $siteUrl $listTitle $desc $templateType
}

#--------------------------------------------------------------------------------------------------------------------------------------
# ISSUETRACKLIST
# Function to create TASK list
# PARAMETERS: listTitle, desc, siteUrl
#--------------------------------------------------------------------------------------------------------------------------------------
function taskList([string]$listTitle,[string] $desc, [string] $siteUrl)
{
    $templateType = "tasks"
    createList $siteUrl $listTitle $desc $templateType
}

#--------------------------------------------------------------------------------------------------------------------------------------
# DOCUMENTLIST
# Function to create Document list
# PARAMETERS: listTitle, desc, siteUrl
#--------------------------------------------------------------------------------------------------------------------------------------
function documentList([string]$listTitle,[string] $desc, [string] $siteUrl)
{
    $templateType = "DocumentLibrary"
    createList $siteUrl $listTitle $desc $templateType
}

#--------------------------------------------------------------------------------------------------------------------------------------
# ANNOUNCEMENTLIST
# Function to create ANNOUNCEMENT list
# PARAMETERS: listTitle, desc, siteUrl
#--------------------------------------------------------------------------------------------------------------------------------------
function AnnouncementList([string]$listTitle,[string] $desc, [string] $siteUrl)
{
    $templateType = "announcement"
    createList $siteUrl $listTitle $desc $templateType
}

#--------------------------------------------------------------------------------------------------------------------------------------
# WIKILIST
# Function to create WIKI list
# PARAMETERS: listTitle, desc, siteUrl
#--------------------------------------------------------------------------------------------------------------------------------------
function WikiList([string]$listTitle,[string] $desc, [string] $siteUrl)
{
    $templateType = "webpagelib"
    createList $siteUrl $listTitle $desc $templateType
}

#--------------------------------------------------------------------------------------------------------------------------------------
# CREATELIST
# Function which create any type of Lists that are specified above such as Picture or Document
# PARAM REQUIRED: siteUrl, listTitle, templateType, quickLaunch
#--------------------------------------------------------------------------------------------------------------------------------------
function createList([string]$siteUrl, [string]$listTitle, [string] $desc, [Microsoft.SharePoint.Client.ListTemplateType]$templateType, [Microsoft.SharePoint.Client.QuickLaunchOptions]$quickLaunch = "DefaultValue")
{
    Write-Host "Define Client Context object based on URL given" -BackgroundColor Green -ForegroundColor Black
    $ctx = defineContext $siteUrl

    #if the list is not exist, then create new one
    if($listResult.Title -eq $null)
    {
        Write-Host "Define List properties" -BackgroundColor Cyan -ForegroundColor Black       
        $listCreationInfo = New-Object Microsoft.SharePoint.Client.ListCreationInformation
        $listCreationInfo.TemplateType = $templateType
        Write-Host $templateType
        Write-Host $listCreationInfo.TemplateType
        $listCreationInfo.Title = $listTitle
        Write-Host $listTitle
        Write-Host $listCreationInfo.Title
        $listCreationInfo.QuickLaunchOption = $quickLaunch
        Write-Host $quickLaunch
        Write-Host $listCreationInfo.QuickLaunchOption
        $listCreationInfo.Description = $desc
        Write-Host $listCreationInfo.Description
        $list = $ctx.Web.Lists.Add($listCreationInfo)  

        try
        {
            Write-Host "Creating List object and its properties" -ForegroundColor Cyan -BackgroundColor Black
            $ctx.ExecuteQuery();
            write-host "List $listTitle is created successfully" -ForegroundColor Cyan -BackgroundColor Black
        }
        catch
        {
            Write-Host "Can't create List object" -BackgroundColor Red -ForegroundColor Black
        }    
    }
    else
    {
        write-host "The list with that name already exist" -BackgroundColor Red
    }
    
}

#--------------------------------------------------------------------------------------------------------------------------------------
#TESTLIST
#Is used to test whether the list is exist or not
#--------------------------------------------------------------------------------------------------------------------------------------
function testList([string]$listName)
{
    #$listName = "Documents"
    $lists = $ctx.Web.Lists
    $ctx.Load($lists)
    exeCtx $ctx

    $result = $lists | select -ExpandProperty Title
    $exists = ($result -contains $listName)
    return $exists
}
#--------------------------------------------------------------------------------------------------------------------------------------
#TESTSPECIFICCT
#Is used to test whether certain CT is exist using $PARAM provided.
#It returns true / false
#--------------------------------------------------------------------------------------------------------------------------------------
function testSpecificCT([string]$ctName)
{
    $cts = $ctx.Web.ContentTypes
    $ctx.Load($cts)
    exeCtx $ctx
    $result = $cts | select -ExpandProperty Name
    $exists = ($result -contains $ctName)
    return $exists
}

#--------------------------------------------------------------------------------------------------------------------------------------
# TESTCT
# function to test if the particular CT exist using $PARAM provided.
# It returns TRUE / FALSE 
#--------------------------------------------------------------------------------------------------------------------------------------
function testCT([string]$ctName)
{
    #$ctName = "Document"
    $cts = $gWeb.ContentTypes
    $ctx.Load($cts)
    $ctx.ExecuteQuery()
    $result = $cts | select -ExpandProperty Name
    $exists = ($result -contains $ctName)
    return $exists
}

#--------------------------------------------------------------------------------------------------------------------------------------
# DEFINECONTEXT
# function to redefine client context object.
# It is used because List function require change URL frequently
#--------------------------------------------------------------------------------------------------------------------------------------
function defineContext([string] $url)
{
    #$url = "https://nsitechnology.sharepoint.com/sites/Engagement"
    $context = New-Object Microsoft.SharePoint.Client.ClientContext($url)
    $context.Credentials = New-Object Microsoft.SharePoint.Client.SharePointOnlineCredentials($Username, $AdminPassword)
    $context.RequestTimeout = 7200000; #Set the timeout to be 2 hours at least    
    #Execute Query
    exeCtx $context
        
    return $context
}

#--------------------------------------------------------------------------------------------------------------------------------------
# EXECTX
# function to execute ClientContext object.
# It is used because List function require change URL frequently.
#--------------------------------------------------------------------------------------------------------------------------------------
function exeCtx([Microsoft.SharePoint.Client.ClientContext] $ctxParam)
{
	try
	{
		$ctxParam.ExecuteQuery();
	}
	catch [Net.WebException]
	{
		write-host "Can't instantiate client object"
		write-host $_.Exception.ToString()
	}
}