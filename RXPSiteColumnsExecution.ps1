﻿. "C:\projects\RxPInternalProductionScript\RXPSiteColumns.ps1"

#Create Single Line of text Site column
for($i = 0; $i -ne $singleLineVar.Count; $i++)
{
    write-host "Creating new Single Line Plain Text Site Columns" -BackgroundColor Cyan -ForegroundColor Black

    #Pass 3 arguments
    textSiteColumn $singleLineVar[$i] $singleLineVarSetting[$i] $singleLineVarDesc[$i]
    
    Write-Host "Site Column: $singleLineVar[$i] has created in $AdminUrl"
}

#Create Multi Line enhance text site column
for($i = 0; $i -ne $multiLineVar.Count; $i++)
{
    Write-Host "Creating new multi Line enhance Text Site Columns" -BackgroundColor Cyan -ForegroundColor Black
    #Pass 3 arguments
    enhanceMultiLineTextColumn $multiLineVar[$i] $multiLineVarSetting[$i] $multiLineVarDesc[$i]

    Write-Host "Site column: $multiLineVar[$i] has created in $AdminUrl"
}

#Create Date only site column
for($i =0; $i -ne $dateVar.Count; $i++)
{
    Write-Host "Creating Date Only Site Columns" -BackgroundColor Cyan -ForegroundColor Black
    dateSiteColumn $dateVar[$i] $dateVarSetting[$i] $dateVarDesc[$i]

    Write-Host "Site column: $dateVar[$i] has created in $AdminUrl"
}

#Create DateTime site column
for($i= 0; $i -ne $dateTimeVar.Count; $i++)
{
    Write-Host "Creating Date Time site column" -BackgroundColor Cyan -ForegroundColor Black
    dateTimeSiteColumn $dateTimeVar[$i] $dateTimeVarSetting[$i] $dateTimeVarDesc[$i]

    Write-Host "Site Column: $dateTimeVar[$i] has created in $AdminUrl"
}

#Create MMS Site Column
for($i=0; $i -ne $mmsVar.Count; $i++)
{
    Write-Host "Create Metadata Site Column" -BackgroundColor Cyan -ForegroundColor Black
    metadataSiteColumn $mmsVar[$i] $mmsVarReqSetting[$i] $mmsVarMultSetting[$i] $mmsVarDesc[$i]
}

#Create YesNo choice site column
yesNoChoiceSiteColumn $yesNoVar $yesNoVarDesc

#Create Currency site column
currencySiteColumn $currencyVar $currencyVarSetting $currencyVarDesc

#Material Req listbox
choiceSiteColumn $materialReqName $materialReqVal $format $materalRegSetting $materialReqDesc

#Request Status
choiceSiteColumn $RequestStatusName $RequestStatusVal $RequestStatusFormat $RequestStatusSetting $RequestStatusDesc

#Create People Picker 
for($i=0; $i -ne $peoplePickerName.Count; $i++)
{
    Write-Host "Creating People picker" -BackgroundColor Cyan -ForegroundColor Black
    userSiteColumn $peoplePickerName[$i] $peoplePickerMult[$i] $peoplePickerSelectMode[$i]
    Write-Host "People picker: $peoplePickerName[$i] has created"
}

#Create Hyperlink column
hyperlinkColumn $urlVarName

#Create Policy
metadataSiteColumn $corpColName $corpReq $corpMultVal $corpColDesc

#Create Strategy
metadataSiteColumn $corpStrategyColName $corpStrategyReq $corpStrategyMultVal $corpStrategyColDesc

#Create Annual Report column
metadataSiteColumn $corpAnnualColName $corpAnnualReq $corpAnnualMultVal $corpAnnualColDesc

#--------------------------------------------------------------------------------------------------------------------------------------
#DEV testing
#--------------------------------------------------------------------------------------------------------------------------------------
<#removeBulkSC $singleLineVar
removeBulkSC $multiLineVar
removeBulkSC $dateVar
removeBulkSC $dateTimeVar
removeBulkSC $yesNoVar
removeBulkSC $currencyVar#>
#removeBulkSC $mmsVar

#Add Site Columns into CT Purchase Order
#Site Columns: Purchase Order Number
addBulkSCtoCT $POscs $ctPO

#Add Site Columns into CT Status Report
#Site Columns: Period Start, Period End
addBulkSCtoCT $SRscs $ctSR

#Add Site Columns into CT Financial Document
#Site Columns: Financial Period
addBulkSCtoCT $FDscs $ctFD

#Add Site Columns into CT Emails
#Site Columns: Attachment, BCC, BCC-Address, Categories, CC, CC-Address, From, From-Address, 
#              "Received", "Sent", "Subject", "To", "To-Address"
addBulkSCtoCT $Emailscs $ctEmail

#Add Site Columns into CT Contract Document
#Site Columns: Contract With, Commencement Date, Contract Status, Value
addBulkSCtoCT $ContractDocscs $ctContractDoc

#Add Site Columns into CT "Contract" SubType of Contract Document
#Site Columns: Termination Date
addBulkSCtoCT $contractscs $ctContract

#Add Site Columns into CT Meeting Document
#Site Columns: Start Date, End Date, "Location
addBulkSCtoCT $meetingscs $ctMeetingDoc

#Add Site Columns into CT Reference Document
#Site Columns:Reference Type, Publisher, Discipline, Product or Service, Source
addBulkSCtoCT $referenceDocscs $ctReferenceDoc

#Add Site Columns into CT Marketing Document
#Site Columns: Product or Service, Capability, "Practice
addBulkSCtoCT $ctMarketingDocscs $ctMarketingDoc

#Add Site Columns into CT Case Study
#Site Columns: Client
addBulkSCtoCT $ctCaseStudyscs $ctCaseStudy

#Add Site Columns into CT Consultant Profile
#Site Columns: Consultant, Region
addBulkSCtoCT $ctConsultantProfilescs $ctConsultantProfile

#Add Site Columns into CT RXP Image
#Site Columns: Product or Service", Capability, "Practice
addBulkSCtoCT $ctRXPImagescs $ctRXPImage

addBulkSCtoCT $ctContractDocSetscs $ctContractDocSet

addBulkSCtoCT $ctMeetingDocSetscs $ctMeetingDocSet

addBulkSCtoCT $ctPersonnelRecordscs $ctPersonnelRecord

addBulkSCtoCT $ctEmploymentContractscs $ctEmploymentContract

addBulkSCtoCT $ctPersonnelDocumentscs $ctPersonnelDocument

addBulkSCtoCT $ctMarketingBriefscs $ctMarketingBrief

addBulkSCtoCT $corpColName $rxpCorpCT

#Add Strategy Col into Strategy content type
addBulkSCtoCT $corpStrategyColName $rxpCorpStrategyCT

#Add Annual Site Column into Annual Report content type
addBulkSCtoCT $corpAnnualColName $rxpCorpAnnualCT
